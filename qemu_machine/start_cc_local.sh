QCOW2=/home/esposem/.local/share/libvirt/images/fedora-CVM.qcow2
SECBOOT=.secboot

sudo qemu-kvm \
-name azure-cvm \
-smp 28 \
-machine q35,smm=on,accel=kvm,kernel-irqchip=split \
-global driver=cfi.pflash01,property=secure,value=on \
-cpu host -drive id=drive_image2,if=none,snapshot=off,aio=threads,cache=none,format=qcow2,file=$QCOW2 \
-drive file=/usr/share/edk2/ovmf/OVMF_CODE$SECBOOT.fd,if=pflash,format=raw,readonly=on,unit=0 \
-drive file=~/dm_verity/vars/OVMF_VARS$SECBOOT.fd,if=pflash,format=raw,unit=1 \
-device virtio-blk-pci,id=image2,drive=drive_image2,bootindex=3,serial=SYSTEM_DISK1,bus=pcie.0,addr=0x8 \
-device ahci,id=ahci0 \
-m 32G \
-boot menu=on \
-nic bridge,br=virbr0,helper=/usr/libexec/qemu-bridge-helper \
-net user,hostfwd=tcp::10022-:22 \
-net nic \
-smbios type=11,value=io.systemd.stub.kernel-cmdline-extra=NONSAFE \
--nographic