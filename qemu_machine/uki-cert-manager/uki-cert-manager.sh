#! /bin/bash

PROGRAM_DIR=~/.ovmf_cert_manager

usage() {
    echo "Usage: $0 VERB [-h] [-o KEYS_FOLDER] [-d manager_dir] [-c CERT_COMMON_NAME] [-n CERT_NICKNAME]
"
    echo "$0 creates and manages certificates. Must be run on a trusted non-production machine, as the private key can be extracted from the machine running this script by just specifiying the cert nickname.

Always provide the target machine/VM secureboot db only the PEM/DER certificate files!
"
    echo "Verbs:
    * add: creates a certificate. Accepts options -o,-d,-c,-n,-r.
    * get: get a certificate keys (private, public, pem, der). Accepts options -o,-c,-n.
    * rm: removes a certificate. Accepts options -d,-c,-n.
    * list: list all certificates (name and description) installed with this tool. Accepts option -d.
Options:
    -h : show this help message.
    -d manager_dir: where ovmf_cert_manager folder is located/should be created. Default is $PROGRAM_DIR
    -c : certificate common name.
    -n : certificate nickname.
    -r : when creating a certificate, do not store it on this machine (efikeygen does it by default).
    -o KEYS_FOLDER: store the keys in KEYS_FOLDER.
Environmental variables:
    These variables can be set to avoid inserting commands on input
    CERT_COMMON_NAME (alternative to -c option)
    CERT_NICKNAME (alternative to -n option)
    KEYS_FOLDER (alternative to -o option)
    DELETE_ON_ADD (alternative to -r option)
"
}


get_variable() {
    echo -n "$1"
    read var
    while true; do
        if [[ -n "$var" ]]; then
            eval "$2=\"${var}\""
            return 0
        else
            echo -n "$1"
            read var
        fi
    done
}


get_keys() {
    echo "Getting certificates from secureboot entry in /etc/pki/pesign..."

    if [ -z $KEYS_FOLDER ]; then
        echo "Error: provide -o KEYS_FOLDER or env var KEYS_FOLDER"
        exit 1
    fi

    if [ -z $CERT_NICKNAME ]; then
        get_variable "Enter certificate nickname: " CERT_NICKNAME
    fi

    # if [ ! -z $CERT_COMMON_NAME ]; then
    #     # get_variable "Enter certificate common name: " CERT_COMMON_NAME
    #     key_present=$(keyctl show %:.platform | grep "$CERT_COMMON_NAME")
    #     if [ $? != 0 ]; then
    #         echo "Warning: key $CERT_COMMON_NAME not present!"
    #     else
    #         echo "Printing all keys..."
    #         keyctl show %:.platform
    #     fi
    # fi

    cert_present=$(certutil -K -d /etc/pki/pesign -a | grep "$CERT_NICKNAME")
    if [ $? != 0 ]; then
        echo "Certificate $CERT_NICKNAME not present in /etc/pki/pesign! Aborting"
        exit 1
    fi
    echo "Printing all certificates..."
    certutil -K -d /etc/pki/pesign -a

    pk12util -o $KEYS_FOLDER/keys.p12 -n "${CERT_NICKNAME}" -d /etc/pki/pesign -W 'guest_pw'
    echo "pw of $KEYS_FOLDER/keys.p12 is guest_pw"

    openssl pkcs12 -in $KEYS_FOLDER/keys.p12 -out $KEYS_FOLDER/private.key -nodes -password 'pass:guest_pw'
    echo "Private key generated in $KEYS_FOLDER/private.key"

    openssl rsa -in $KEYS_FOLDER/private.key -outform PEM -pubout -out  $KEYS_FOLDER/public.key
    echo "Public key generated in $KEYS_FOLDER/public.key"

    certutil -d /etc/pki/pesign -n "${CERT_NICKNAME}" -Lr > $KEYS_FOLDER/certificate.der
    echo "DER certificate generated in $KEYS_FOLDER/certificate.der"

    openssl x509 -inform DER -outform PEM -in $KEYS_FOLDER/certificate.der -out $KEYS_FOLDER/certificate.pem
    echo "PEM certificate generated in $KEYS_FOLDER/certificate.pem"
    # inspect with openssl x509 -in $KEYS_FOLDER/certificate.pem -noout -text
}


delete_certificate() {
    if [ -z $CERT_NICKNAME ]; then
        get_variable "Enter certificate nickname: " CERT_NICKNAME
    fi

    certutil -F -n "${CERT_NICKNAME}" -d /etc/pki/pesign
    echo "Certificate $CERT_NICKNAME successfully deleted"

    if [ ! -z $CERT_COMMON_NAME ]; then
        rm -rf $PROGRAM_DIR/$CERT_COMMON_NAME
        echo "Folder $PROGRAM_DIR/$CERT_COMMON_NAME successfully deleted"
    fi
}


create_certificate() {
    if [ -z $CERT_COMMON_NAME ]; then
        get_variable "Enter certificate common name: " CERT_COMMON_NAME
    fi

    if [ -z $CERT_NICKNAME ]; then
        get_variable "Enter certificate nickname: " CERT_NICKNAME
    fi

    echo "CERT_COMMON_NAME: $CERT_COMMON_NAME"
    echo "CERT_NICKNAME: $CERT_NICKNAME"
    echo "KEYS_FOLDER: $KEYS_FOLDER"

    mkdir -p $PROGRAM_DIR/$CERT_COMMON_NAME

    efikeygen -d /etc/pki/pesign -S -k -c "CN=${CERT_COMMON_NAME}" -n "${CERT_NICKNAME}"
    echo "efikeygen performed"

    certutil -d /etc/pki/pesign -n "${CERT_NICKNAME}" -Lr > $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.der
    echo "DER certificate generated in $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.der"

    openssl x509 -inform DER -outform PEM -in $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.der -out $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.pem
    echo "PEM certificate generated in $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.pem"

    echo "COMMON_NAME: ${CERT_COMMON_NAME}" > $PROGRAM_DIR/$CERT_COMMON_NAME/info
    echo "NICKNAME: ${CERT_NICKNAME}" >> $PROGRAM_DIR/$CERT_COMMON_NAME/info
    echo "Info file $PROGRAM_DIR/$CERT_COMMON_NAME/info successfully created"


    if [ ! -z $KEYS_FOLDER ]; then
        cp $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.der $KEYS_FOLDER
        echo "DER certificate copied in $KEYS_FOLDER/sb_cert.der"

        cp $PROGRAM_DIR/$CERT_COMMON_NAME/sb_cert.pem $KEYS_FOLDER
        echo "PEM certificate copied in $KEYS_FOLDER/sb_cert.pem"
    fi

    if [ ! -z $DELETE_ON_ADD ]; then
        delete_certificate
    fi
}


list_certificates() {
    for file in $PROGRAM_DIR/*; do
        echo $file
        if [ -d $file ]; then
            ls $file | sed 's/^/    /'
            echo "    ----"
            cat $file/info | sed 's/^/    /'
        else
            cat $file | sed 's/^/    /'
        fi
    done
}


if [ $# == 0 ]; then
    usage
    exit 1
fi

verb=$1
shift

while getopts ":i:o:u:d:hn:c:o:" o; do
    case "${o}" in
        d)
            s=${OPTARG}
            if [ ! -d $s ]; then
                echo "Error: provided ovmf_cert_manager does not exist"
                usage
                exit 1
            fi
            PROGRAM_DIR=$s
            ;;
        h)
            usage
            exit 0
            ;;
        c)
            CERT_COMMON_NAME=${OPTARG}
            ;;
        n)
            CERT_NICKNAME=${OPTARG}
            ;;
        o)
            s=${OPTARG}
            if [ ! -d $s ]; then
                echo "Error: provided KEYS_FOLDER does not exist"
                usage
                exit 1
            fi
            KEYS_FOLDER=$s
            ;;
        r)
            DELETE_ON_ADD=1
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done


check_package_exist() {
    for var in "$@"; do
        x=$(rpm -qa | grep $var)
        if [ $? != 0 ]; then
            echo "Install required package $var"
            exit 1
        fi
    done
}

echo "Checking if required packages are installed..."
check_package_exist pesign nss-tools virt-firmware keyutils openssl


case $verb in
    "add")
        create_certificate;;
    "get")
        get_keys;;
    "list")
        list_certificates;;
    "rm")
        delete_certificate;;
    *)
        usage
        exit 1;;
esac

echo "Bye!"
exit 0
