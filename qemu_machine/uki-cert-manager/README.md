# uki-cert-manager.sh
An utility to create and manage certificates.

## How to use it and what to know before using it

This tool has to be used in a trusted machine. The certificate create by this tools in PEM/DER format can be safely given to production secureboot dbs, as they just contain the public key of the certificate.

You can inspect a certificate with `openssl x509 -in certificate.pem -noout -text`.

When a certificate is created without the option `-r`, `efikeygen` the tool that internally creates the cert will also add it as an entry to the local secureboot db in /etc/pki/pesign.
This is useful because later we can run `./uki-cert-manager.sh get` and get the private and public keys from the certificate, but also dangerous because with private keys one can sign malicious addons or UKIs.

Therefore the tool has to be used in a trusted machine.

## Command line usage
```
Usage: ./uki-cert-manager.sh VERB [-h] [-o KEYS_FOLDER] [-d manager_dir] [-c CERT_COMMON_NAME] [-n CERT_NICKNAME]

./uki-cert-manager.sh creates and manages certificates. Must be run on a trusted non-production machine, as the private key can be extracted from the machine running this script by just specifiying the cert nickname.

Always provide the target machine/VM secureboot db only the PEM/DER certificate files!

Verbs:
    * add: creates a certificate. Accepts options -o,-d,-c,-n,-r.
    * get: get a certificate keys (private, public, pem, der). Accepts options -o,-c,-n.
    * rm: removes a certificate. Accepts options -d,-c,-n.
    * list: list all certificates (name and description) installed with this tool. Accepts option -d.
Options:
    -h : show this help message.
    -d manager_dir: where ovmf_cert_manager folder is located/should be created. Default is ~/.ovmf_cert_manager
    -c : certificate common name.
    -n : certificate nickname.
    -r : when creating a certificate, do not store it on this machine (efikeygen does it by default).
    -o KEYS_FOLDER: store the keys in KEYS_FOLDER.
Environmental variables:
    These variables can be set to avoid inserting commands on input
    CERT_COMMON_NAME (alternative to -c option)
    CERT_NICKNAME (alternative to -n option)
    KEYS_FOLDER (alternative to -o option)
    DELETE_ON_ADD (alternative to -r option)
```

## Adding cert to MOK
Not really recommended, but if you want to add the key to MOK, do the following:

1. create a key with `./uki-cert-manager.sh add -c MY_COMMON_NAME -n MY_NICKNAME -o MY_KEYS_FOLDER`. Important: do not provide `-r`.
2. mokutil --import MY_KEYS_FOLDER/sb_cert.der # secureboot must be on
3. reboot
4. Click on "Enroll MOK"
5. Click any key to enter the MOK manager menu
6. View key
7. Press enter
8. Confirm the key has to be added by selecting "Yes" and pressing enter
9. Add the password created before for the certificate
10. Wait that the system reboots
11. Check key is present: `keyctl show %:.platform | grep MY_COMMON_NAME` and `certutil -K -d /etc/pki/pesign -a | grep MY_NICKNAME`
12. Delete key from pesign and locally `./uki-cert-manager.sh rm -c MY_COMMON_NAME -n MY_NICKNAME`
13. Move the keys in MY_KEYS_FOLDER somewhere else

## Removing cert from MOK

1. `./uki-cert-manager.sh rm -c MY_COMMON_NAME -n MY_NICKNAME`
2. `mokutil --delete MY_KEYS_FOLDER/sb_cert.der`
3. reboot
4. Click any key to enter the MOK manager menu
5. Click on "Remove MOK"
6. View key
7. Press enter
8. Confirm the key has to be deleted by selecting "Yes" and pressing enter
9. Add the password created before for the certificate
10. Wait that the system reboots
11. Check key is empty: `keyctl show %:.platform | grep MY_COMMON_NAME` and `certutil -K -d /etc/pki/pesign -a | grep MY_NICKNAME`
