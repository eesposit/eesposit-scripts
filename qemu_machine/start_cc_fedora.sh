#!/bin/bash

QCOW2=~/.local/share/libvirt/images/Fedora-Cloud-Base-UEFI-UKI.x86_64-Rawhide-20240410.n.0.qcow2
# QCOW2=/var/lib/libvirt/images/fedora-rh.qcow2

# sudo dnf install swtpm swtpm-tools

cd ~/kernel/vms

swtpm socket --tpmstate dir=sock/tpm0 \
    --ctrl type=unixio,path=sock/tpm0/swtpm-sock \
    --log level=40 --tpm2 -t -d

sudo ./virtiofsd --socket-path=sock/vm001-vhost-fs.sock  -o source=./shared_host_guest/ &

SECBOOT=.secboot
#SECBOOT=""

sudo /usr/libexec/qemu-kvm \
-name azure-cvm \
-smp 28 \
-machine q35,smm=on,accel=kvm,kernel-irqchip=split \
-global driver=cfi.pflash01,property=secure,value=on \
-cpu host -drive id=drive_image2,if=none,snapshot=off,aio=threads,cache=none,format=qcow2,file=$QCOW2 \
-drive file=/usr/share/edk2/ovmf/OVMF_CODE$SECBOOT.fd,if=pflash,format=raw,readonly=on,unit=0 \
-drive file=vars/fedora/OVMF_VARS$SECBOOT.fd,if=pflash,format=raw,unit=1 \
-device virtio-blk-pci,id=image2,drive=drive_image2,bootindex=3,serial=SYSTEM_DISK1,bus=pcie.0,addr=0x8 \
-device ahci,id=ahci0 \
-m 32G \
-boot menu=on \
-nic bridge,br=virbr0,helper=/usr/libexec/qemu-bridge-helper \
-chardev socket,id=char0,path=sock/vm001-vhost-fs.sock \
-device vhost-user-fs-pci,chardev=char0,tag=myfs \
-object memory-backend-memfd,id=mem,size=32G,share=on \
-numa node,memdev=mem \
-net user,hostfwd=tcp::10023-:23 \
-net nic \
-smbios type=11,value=io.systemd.stub.kernel-cmdline-extra=NONSAFE \
-chardev socket,id=chrtpm,path=sock/tpm0/swtpm-sock \
-tpmdev emulator,id=tpm0,chardev=chrtpm -device tpm-tis,tpmdev=tpm0 \
--nographic

# -nodefaults -serial file:qemu_log
# -debugcon file:debug.log -global isa-debugcon.iobase=0x402 


