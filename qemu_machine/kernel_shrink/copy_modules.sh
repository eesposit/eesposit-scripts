#! /bin/bash

kernel=`uname -r`
source /etc/os-release

modprobe overlay

mkdir -p /run/extensions/modules_ext/usr/lib/extension-release.d
mkdir -p /run/extensions/modules_ext/usr/lib/modules
echo ID=$ID > /run/extensions/modules_ext/usr/lib/extension-release.d/extension-release.modules_ext
echo VERSION_ID=$VERSION_ID >> /run/extensions/modules_ext/usr/lib/extension-release.d/extension-release.modules_ext
echo SYSEXT_SCOPE=system >> /run/extensions/modules_ext/usr/lib/extension-release.d/extension-release.modules_ext
mv /usr/lib/modules/$kernel /run/extensions/modules_ext/usr/lib/modules