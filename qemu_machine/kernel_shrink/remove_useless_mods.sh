#! /bin/bash

OUT_FILE=$1

if [ -z $1 ]; then
    echo "Usage: $0 out_file"
    exit 1
fi

BLACKLIST=("hv_netvsc" "hv_balloon")

unused_mod=$(lsmod | grep " 0" | cut -f 1 -d ' ')

while [ ! -z "$unused_mod" ]; do
    for mod in $unused_mod; do
        found=false
        for blacklisted in "${BLACKLIST[@]}"; do
            if [ $blacklisted == $mod ]; then
                    found=true
            fi
        done
        if ! $found; then
            # sudo rmmod $mod 2> /dev/null
            echo " - $mod removed";
            echo $mod >> $OUT_FILE
        fi
    done
    unused_mod=$(lsmod | grep " 0" | cut -f 1 -d ' ')
    unused_mod=""
done
