#!/usr/bin/env python3

import sys
import subprocess

modules = set()

def uname():
    return subprocess.check_output('uname -r'.split(), text=True).strip()

def check_module_exist(module):
    x = subprocess.check_output(f'find /lib/modules -name *.ko.xz'.split(), text=True).strip()
    return x.find(f'/{module}.ko.xz') != -1

def parse_file(path):
    with open(path, 'r') as modules_f:
        for mod in modules_f.readlines():
            mod = mod.strip()
            mod_= mod.replace('-', '_')
            if check_module_exist(mod_):
                modules.add(mod_)
                continue
            mod= mod.replace('_', '-')
            if check_module_exist(mod):
                modules.add(mod)
                continue
            print(f'Module {mod}/{mod_} does not exist!')

def write_out(path):
    with open(path, 'w') as out:
        for mod in sorted(modules):
            out.write(mod + '\n')

if __name__ == "__main__":
    out = sys.argv[1]
    for file in sys.argv[2:]:
        parse_file(file)

    write_out(out)
