#! /bin/bash
set -e

UKIFY="python3 /root/systemd/src/ukify/ukify.py build"
STUB="--stub=/root/systemd_build/src/boot/efi/addonx64.efi.stub"

UNAME=$(uname -r)
INVALID_UNAME='invalid UNAME'

DEVEL_DIR=/boot/efi/EFI/Linux/devel.efi.extra.d
INVALID_DIR=/boot/efi/EFI/Linux/invalid.efi.extra.d
GENERIC_DIR=/boot/efi/loader/addons

VALID_SBAT="sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
uki.addon,2,UKI Addon,uki.addon,1,https://www.freedesktop.org/software/systemd/man/systemd-stub.html"

mkdir -p $DEVEL_DIR
mkdir -p $INVALID_DIR
mkdir -p $GENERIC_DIR

rm -f $DEVEL_DIR/*
rm -f $INVALID_DIR/*
rm -f $GENERIC_DIR/*

# ok
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|devel-UKI specific cmdline signed|' --output=$DEVEL_DIR/test.addon.efi --sbat="$VALID_SBAT"
# rejected invalid sbat
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|devel-UKI specific cmdline signed INVALID SBAT|' --output=$DEVEL_DIR/test-sbat.addon.efi
# rejected unsigned
$UKIFY $STUB --cmdline='|devel-UKI specific cmdline NOT signed|' --output=$DEVEL_DIR/test-unsigned.addon.efi --sbat="$VALID_SBAT"
# ok
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|devel-UKI specific cmdline signed uname|' --output=$DEVEL_DIR/test-uname.addon.efi --uname=$UNAME --sbat="$VALID_SBAT"
# rejected invalid sbat
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|devel-UKI specific cmdline signed uname INVALID SBAT|' --output=$DEVEL_DIR/test-uname-sbat.addon.efi --uname=$UNAME
# rejected uname mismatch
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|devel-UKI specific cmdline invalid uname|' --output=$DEVEL_DIR/test-invalid-uname.addon.efi --uname="$INVALID_UNAME" --sbat="$VALID_SBAT"

# completely ignored
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|invalid-UKI specific cmdline signed|' --output=$INVALID_DIR/test.addon.efi
$UKIFY $STUB --cmdline='|invalid-UKI specific cmdline NOT signed|' --output=$INVALID_DIR/test-unsigned.addon.efi
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|invalid-UKI specific cmdline signed uname|' --output=$INVALID_DIR/test-uname.addon.efi --uname=$UNAME
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|invalid-UKI specific cmdline invalid uname|' --output=$INVALID_DIR/test-invalid-uname.addon.efi --uname="$INVALID_UNAME"

# ok
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|Generic cmdline signed|' --output=$GENERIC_DIR/test.addon.efi --sbat="$VALID_SBAT"
# rejected invalid sbat
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|Generic cmdline signed INVALID SBAT|' --output=$GENERIC_DIR/test-sbat.addon.efi
# rejected unsigned
$UKIFY $STUB --cmdline='|Generic cmdline NOT signed|' --output=$GENERIC_DIR/test-unsigned.addon.efi --sbat="$VALID_SBAT"
# ok
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|Generic cmdline signed uname|' --output=$GENERIC_DIR/test-uname.addon.efi --uname=$UNAME --sbat="$VALID_SBAT"
# rejected invalid sbat
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|Generic cmdline signed uname INVALID SBAT|' --output=$GENERIC_DIR/test-uname-sbat.addon.efi --uname=$UNAME
# rejected uname mismatch
$UKIFY --signtool=pesign --secureboot-certificate-name='Signed UKI' $STUB --cmdline='|Generic cmdline invalid uname|' --output=$GENERIC_DIR/test-invalid-uname.addon.efi --uname="$INVALID_UNAME" --sbat="$VALID_SBAT"

# expected command line:
# |Generic cmdline signed uname| |Generic cmdline signed| |devel-UKI specific cmdline signed uname| |devel-UKI specific cmdline signed|