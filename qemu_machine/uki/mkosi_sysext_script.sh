#! /bin/bash

FOLDER=${FOLDER:-"/tmp/mkosi"}
EXT_NAME=${EXT_NAME:-"btrfs"}

# CRT=${CRT:="/root/.ovmf_cert_manager/UKI/mkosi.crt"}
# KEY=${KEY:="/root/.ovmf_cert_manager/UKI/mkosi.key"}

# source /etc/os-release
# EXT_ID=$ID
# EXT_VERSION=$VERSION_ID
# EXT_SCOPE=${EXT_SCOPE:-"initrd"}

if [ ! -z "$(ls -A $FOLDER 2> /dev/null)" ]; then
	echo "$FOLDER exist and is not empty, use another one."
	exit 1
fi

mkdir -p $FOLDER
cd $FOLDER

echo "[Output]
OutputDirectory=mkosi.output
CacheDirectory=mkosi.cache" > mkosi.conf

mkdir -p mkosi.images/base

echo "[Output]
Format=directory

[Content]
CleanPackageMetadata=no
Packages=systemd
         udev" > mkosi.images/base/mkosi.conf

mkdir -p mkosi.images/$EXT_NAME

# TODO: delete btrfs-progs
echo "[Config]
Dependencies=base

[Output]
Format=sysext
Overlay=yes

[Content]
BaseTrees=%O/base
Packages=btrfs-progs" > mkosi.images/$EXT_NAME/mkosi.conf


mkdir -p mkosi.images/$EXT_NAME/mkosi.extra/usr
# example file
touch mkosi.images/$EXT_NAME/mkosi.extra/usr/ciao.txt

# create a drop-in in tmpfiles.d to create a file in /run/, so that the user
# after switching to root can see that the extension was loaded.
# mkdir -p mkosi.images/$EXT_NAME/mkosi.extra/usr/lib/tmpfiles.d

# echo "f /run/addon_created 0755 - - -" > mkosi.images/$EXT_NAME/mkosi.extra/usr/lib/tmpfiles.d/sysext-addon.conf

# mkdir -p mkosi.images/$EXT_NAME/mkosi.repart

# echo "[Partition]
# Type=root
# Format=squashfs
# CopyFiles=/usr/
# Verity=data
# VerityMatchKey=root
# Minimize=best" > mkosi.images/$EXT_NAME/mkosi.repart/10-root.conf

# echo "[Partition]
# Type=root-verity
# Verity=hash
# VerityMatchKey=root
# Minimize=best" > mkosi.images/$EXT_NAME/mkosi.repart/20-root-verity.conf

# echo "[Partition]
# Type=root-verity-sig
# Verity=signature
# VerityMatchKey=root" > mkosi.images/$EXT_NAME/mkosi.repart/30-root-verity-sig.conf

mkosi genkey
mkdir -p /run/verity.d
cp mkosi.* /run/verity.d

mkdir -p mkosi.images/initrd

echo "[Config]
Dependencies=base
[Output]
Format=cpio
[Content]
MakeInitrd=yes
BaseTrees=%O/base" > mkosi.images/initrd/mkosi.conf

mkosi -f
