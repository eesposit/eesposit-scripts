
1. Create an UKI-backed qcow2, with /usr as separate partition: use /home/eesposit/eesposit-scripts/qemu_machine/uki/rhel9-dm-usr.ks

2. If you want to sign the UKI with your own certificate
Create a custom certificate using uki-cert-manager:
```
cd qemu_machine/uki-cert-manager/
./uki-cert-manager.sh add -c UKI_devel -n UKI_devel

mokutil --import ~/.ovmf_cert_manager/UKI_devel/sb_cert.der
reboot # and follow mokutil screen to add the key
```

Alternatively, disable secureboot
```
sudo mokutil --disable-validation
```

3. Create a dracut config file, use `qemu_machine/uki/dracut-virt.conf`

4. Build your own UKI (change SIGN if you don't want UKI to be signed, and check the script for other options):
```
v=`kernel-bootcfg | grep UKI_devel`
if [ -n "$v" ]; then
	echo "Removing efibootmgr $v"
	kernel-bootcfg --remove-uki /boot/efi/EFI/Linux/devel.efi
fi
bash SIGN=true qemu_machine/uki/load_efi_devel.sh
```

5. If you need /usr verity protected, use `qemu_machine/uki/verity-usr.sh`. Again check env variables at beginning of file

6. Handy command to delete the root (assuming is part 2):
```
QCOW2=~/.local/share/libvirt/images/rhel9.6-dm.qcow2 && sudo qemu-nbd -c /dev/nbd0 -f qcow2 $QCOW2 && sudo sfdisk --delete "/dev/nbd0" "2" && sudo fdisk /dev/nbd0 --list && sudo qemu-nbd --disconnect /dev/nbd0
```
