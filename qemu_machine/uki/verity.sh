#! /bin/bash
set -e

if [ -z ${QCOW2+x} ]; then
    echo "QCOW2 is unset";
    exit
else
    echo "QCOW2 $QCOW2"
fi


VERITY_FOLDER=$(mktemp -d)
EFI_PN=${EFI_PN:-"1"}
ROOT_PN=${ROOT_PN:-"2"}
echo VERITY_FOLDER=$VERITY_FOLDER
echo EFI_PN=$EFI_PN
echo ROOT_PN=$ROOT_PN

here=`pwd`

dir_created=0
nbd_mounted=0

function handle_ctrlc()
{
    if [[ $nbd_mounted == 1 ]]; then
        sudo qemu-nbd --disconnect /dev/nbd0
    fi
    cd $here
    exit
}

# trapping the SIGINT signal
trap handle_ctrlc SIGINT
trap handle_ctrlc EXIT

cd $VERITY_FOLDER
dir_created=1

mkdir mnt

sudo modprobe nbd
sudo qemu-nbd -c /dev/nbd0 -f qcow2 $QCOW2
nbd_mounted=1

# create config files and folders for systemd-repart and UKI
WORKDIR=conf
mkdir $WORKDIR

echo "[Partition]
Type=root-verity
Verity=hash
VerityMatchKey=asd
SizeMinBytes=500M
SizeMaxBytes=1G" > $WORKDIR/verity.conf

echo "[Partition]
Type=root
Verity=data
VerityMatchKey=asd
SizeMinBytes=6G
SizeMaxBytes=6G" > $WORKDIR/root.conf

sudo systemd-repart /dev/nbd0 --dry-run=no --definitions=$WORKDIR --no-pager --json=pretty | jq -r '.[] | select(.type == "root-x86-64-verity") | .roothash' > $WORKDIR/roothash.txt
RH=$(cat $WORKDIR/roothash.txt)

echo "Root hash: $RH"

# prepare addon. EFI is EFI_PN
echo "Creating addon into /boot/efi..."
sudo mount /dev/nbd0p$EFI_PN mnt
cd mnt/EFI/Linux/*.extra.d
sudo rm -f verity.addon.efi
sudo /usr/lib/systemd/ukify build --cmdline="roothash=$RH" --output=verity.addon.efi
sudo /usr/lib/systemd/ukify inspect verity.addon.efi
cd - && sudo umount mnt
echo "Creating addon into /boot/efi OK"

sudo qemu-nbd --disconnect /dev/nbd0
cd $here