########### UKI cloud image
wget https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Cloud/x86_64/images/Fedora-Cloud-Base-UEFI-UKI.x86_64-Rawhide-20240404.n.0.qcow2

qemu-img resize Fedora-Cloud-Base-UEFI-UKI.x86_64-Rawhide-20240410.n.0.qcow2 30G
sudo modprobe nbd max_part=10
sudo qemu-nbd -c /dev/nbd0 Fedora-Cloud-Base-UEFI-UKI.x86_64-Rawhide-20240410.n.0.qcow2
sudo parted /dev/nbd0
(parted) print free
(parted) resizepart
(parted) quit
sudo qemu-nbd -d /dev/nbd0

Fix
mkpart p.verity
ext4
32.2GB
42.9GB
q
sudo mkfs.xfs /dev/vda4

sudo virt-install --virt-type kvm --os-variant fedora-rawhide --arch x86_64 --boot uefi --name fedora-uki --memory 8192  --disk /var/lib/libvirt/images/Fedora-Cloud-Base-UEFI-UKI.x86_64-Rawhide-20240410.n.0.qcow2,bus=scsi,size=30 --cloud-init

once logged in, do `sudo passwd root`

sudo btrfs filesystem resize max /

########### normal iso image
in virtlab machine:
cd /var/lib/libvirt/images/
wget https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Server/x86_64/iso/Fedora-Server-netinst-x86_64-Rawhide-20230919.n.0.iso

in machine with GUI: connect via virt-manager (root user)

in virtlab machine:
sudo -i
virt-install --virt-type kvm --os-variant fedora-rawhide --arch x86_64 --boot uefi --name fedora-rh2 --memory 8192 --location /home/eesposit/.local/share/libvirt/images/Fedora-Server-netinst-x86_64-Rawhide-20230919.n.0.iso --disk bus=scsi,size=30

virt-install --virt-type kvm --os-variant rhel9.0 --arch x86_64 --boot uefi --name rhel9.6-dm --memory 8192 --location ~/.local/share/libvirt/images/isos/RHEL-9.6.0-20241202.2-x86_64-dvd1.iso --disk bus=scsi,size=20 --initrd-inject=rhel9-azure-cvm.ks --nographics --extra-args "console=ttyS0 inst.ks=file:/rhel9-azure-cvm.ks"

in machine with GUI, open virt manager app and click on "fedora-rh" machine. Let installer start, and set:
Installation Destination -> custom -> Done
Partition scheme: Standard partition
Click here to create them automatically -> delete /boot
Create /boot/efi as Standard partition and FS EFI System Partition
check that partition "/" has ext4 as fs type

install
reboot
systemctl enable serial-getty@ttyS0.service
systemctl start serial-getty@ttyS0.service

now you can virsh console fedora-rh

Follow "Run UKI on a QEMU VM" from docs

show VM qemu command line


fdisk -l
fdisk /dev/sda
t
2
23
w

sudo dnf install -y kernel-uki-virt

find the UKI:
ls /boot/efi/EFI/Linux/

###########
If internet doesnt work:  nmcli con up enp1s0

short: sudo dnf install -y systemd-ukify kernel-uki-virt systemd-boot shim efibootmgr uki-direct

ukify:
sudo dnf install -y systemd-ukify
/usr/lib/systemd/ukify

#https://github.com/systemd/systemd/pull/31930/files
#vi /usr/lib/kernel/install.d/60-ukify.install

stub:
sudo dnf install -y systemd-boot
/usr/lib/systemd/boot/efi/linuxx64.efi.stub
/usr/lib/systemd/boot/efi/addonx64.efi.stub

sudo dnf install -y shim efibootmgr
# pip3 install virt-firmware

## not needed in uki cloud image
kernel-bootcfg --add-uki /boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi --title UKI_shim_boot --boot-order 0
------------------ OLD way of doing it
printf "\\\EFI\\\Linux\\\aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi\0" | iconv -f ASCII -t UCS-2 > /tmp/bootarg
efibootmgr -c -d /dev/vda -p 1 --append-binary-args /tmp/bootarg -L "UKI_shim_boot" -l "\EFI\fedora\shimx64.efi"
------------------
#################################

echo "
mount -t virtiofs myfs /mnt
ALIASES_FOLDER=/mnt/eesposit-scripts
source /mnt/eesposit-scripts/zshrc_aliases
source /mnt/eesposit-scripts/qemu_machine/uki/load_efi_Fedora_vars.sh
cd /mnt" >> ~/.bashrc
source ~/.bashrc
bash /mnt/eesposit-scripts/beaker_setup/install_packages.sh

cd eesposit-scripts/qemu_machine/uki-cert-manager/
./uki-cert-manager.sh add -c UKI_devel -n UKI_devel
mokutil --import ~/.ovmf_cert_manager/UKI_devel/sb_cert.der

keyctl show %:.platform

mkdir ~/systemd_build
meson setup -Dbootloader=true -Dukify=true ~/systemd_build
ninja -C ~/systemd_build

# get vmlinux, as it is needed for custom uki
objcopy -O binary --only-section=".linux" /boot/efi/EFI/Linux/6.9.0-0.rc3.20240409git20cb38a7af88.31.fc41.x86_64.efi /boot/vmlinuz-6.9.0-0.rc3.20240409git20cb38a7af88.31.fc41.x86_64

objcopy -O binary --only-section=".initrd" /boot/efi/EFI/Linux/6.9.0-0.rc3.20240409git20cb38a7af88.31.fc41.x86_64.efi /boot/initrd-6.9.0-0.rc3.20240409git20cb38a7af88.31.fc41.x86_64

efibootmgr
kernel-bootcfg

ukify inspect /boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi --section=.cmdline:text

.cmdline:
  size: 29 bytes
  sha256: 5629a039dffb720e992b966a4129340d075c15f70cffc0dd87a51896a4f5021a
  text:
     console=tty0 console=ttyS0


cat /proc/cmdline
BOOT_IMAGE=(hd0,gpt2)/boot/vmlinuz-6.6.0-0.rc1.20230915git9fdfb15a3dbf.17.fc40.x86_64 root=UUID=4cf29f60-9f5b-4284-8d21-87fef4cb22c0 ro rhgb quiet

mkdir /boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi.extra.d
mkdir /boot/efi/EFI/Linux/devel.efi.extra.d

efid

ukify build --signtool=pesign --secureboot-certificate-name='UKI_devel' --cmdline='rd.break' --output=/boot/efi/EFI/Linux/devel.efi.extra.d/rd.addon.efi

### DEMO part ################################################
./uki-cert-manager.sh add -c "UKI" -n "UKI"

cp ~/.ovmf_cert_manager/UKI/sb_cert.pem /mnt/
cp ~/.ovmf_cert_manager/UKI/sb_cert.der /mnt/

#1 from host, embed the key in OVMF_VARS
<Host>
virt-fw-vars -i vars/fedora/OVMF_VARS.secboot.fd -o vars/fedora/OVMF_VARS.secboot.fd --add-db `uuidgen` shared_host_guest/certificate.pem
</Host>

#2 MOK: add with mokutil and reboot
mokutil --import sb_cert.der
reboot

/usr/lib/systemd/ukify build --signtool=pesign --secureboot-certificate-name='UKI' --stub=/usr/lib/systemd/boot/efi/addonx64.efi.stub --cmdline='quiet loglevel=3 systemd.show_status=auto' --output=/boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi.extra.d/quiet.addon.efi

ukify build --signtool=pesign --secureboot-certificate-name='UKI' --cmdline='rd.break' --output=/boot/efi/EFI/Linux/devel.efi.extra.d/rd.addon.efi

ukify inspect /boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi.extra.d/quiet.addon.efi

ukify inspect /boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi.extra.d/quiet.addon.efi --section=.cmdline:text

/usr/lib/systemd/ukify build --signtool=pesign --secureboot-certificate-name='UKI' --stub=/usr/lib/systemd/boot/efi/addonx64.efi.stub --cmdline='ro rhgb quiet crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M' --output=/boot/efi/EFI/Linux/aee61ec8835240c3bed19a53e73a9c1c-6.9.0-0.rc2.23.fc41.x86_64.efi.extra.d/quiet2.addon.efi

bootctl list

bootctl

//TODO sbat example

//todo create uki
use load_efi_devel.sh


rpmdev-setuptree

cd ~/eesposit-scripts/qemu_machine
cp -r uki-host-tools ~/uki-host-tools-0.0.1
cd ~
tar --create --file uki-host-tools-0.0.1.tar.gz uki-host-tools-0.0.1
mv uki-host-tools-0.0.1.tar.gz ~/rpmbuild/SOURCES
cd ~/rpmbuild/SPECS
rpmdev-newspec uki-host-tools

rpmlint ~/rpmbuild/SPECS/uki-host-tools.spec

rpmbuild -bb ~/rpmbuild/SPECS/uki-host-tools.spec

rm -rf ~/uki-host-tools-0.0.1

tree ~/rpmbuild/

x= subprocess.check_output(['cat', '/sys/firmware/efi/efivars/Boot0004-8be4df61-93ca-11d2-aa0d-00e098032b8c'], text=True)



ukify  --signtool=pesign --secureboot-certificate-name='UKI' --cmdline='this is a normal addon' --output=/usr/lib/kernel/addons/rpm_addon.addon.efi
ukify  --signtool=pesign --secureboot-certificate-name='UKI' --cmdline='this is the update addon' --output=/usr/lib/kernel/addons/update.addon.efi
cd /usr/lib/kernel/addons/
mv rpm_addon.addon.efi rpm_addon
mv update.addon.efi rpm_addon.addon.efi
python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --global
python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-title 'UKI_devel'
python addons.py

#  tests for virt-fw uki-addon program
# TODO: python addons.py
# TODO: python addons.py --verbose
# TODO: python addons.py --global
# TODO: python addons.py --global --verbose
# TODO: python addons.py --uki-title 'UKI_devel'
# TODO: python addons.py --uki-title 'UKI_devel' --verbose
# TODO: python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# TODO: python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/ --verbose
# TODO: python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi
# TODO: python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi --verbose
# TODO: python addons.py --show-installed
# TODO: python addons.py --show-installed --verbose

# TODO: python addons.py --list
# TODO: python addons.py --list /boot/efi/EFI/Linux/devel.efi.extra.d/
# TODO: python addons.py --list --verbose
# TODO: python addons.py --list /boot/efi/EFI/Linux/devel.efi.extra.d/ --verbose

# TODO: python addons.py --update-addon # failure
# TODO: python addons.py --update-addon /usr/lib/kernel/addons/rpm_addon.addon.efi # failure
# TODO: python addons.py --update-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --global
# python addons.py --global
# TODO: python addons.py --update-addon /usr/lib/kernel/addons/rpm_addon.addon.efi
# python addons.py
# TODO: python addons.py --update-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-title 'UKI_devel'
# python addons.py --uki-title 'UKI_devel'
# TODO: python addons.py --update-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# TODO: python addons.py --update-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-path /boot/efi/EFI/Linux/devel.efi
# python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi

# TODO: python addons.py --install-addon # failure
# TODO: python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi # failure
# TODO: python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --global
# python addons.py --global
# python addons.py --remove-addon rpm_addon.addon.efi --global
# python addons.py --global
# TODO: python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-title 'UKI_devel'
# python addons.py --uki-title 'UKI_devel'
# python addons.py --remove-addon rpm_addon.addon.efi --uki-title 'UKI_devel'
# python addons.py --uki-title 'UKI_devel'
# TODO: python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# python addons.py --remove-addon rpm_addon.addon.efi --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi.extra.d/
# TODO: python addons.py --install-addon /usr/lib/kernel/addons/rpm_addon.addon.efi --uki-path /boot/efi/EFI/Linux/devel.efi
# python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi
# python addons.py --remove-addon rpm_addon.addon.efi --uki-path /boot/efi/EFI/Linux/devel.efi
# python addons.py --uki-path /boot/efi/EFI/Linux/devel.efi




------

IMPORTANT: REQUIRES SYSTEMD 255

Enable systemd-sysext:
1. add systemd-sysext dracutmodule in dracut.conf
2. add squashfs and erofs driver in dracut.conf
3. add dm-verity driver in dracut.conf
4. Add dracut line install_items+=" /usr/lib/verity.d/mkosi.crt "
5. debug: create addon rd.break

Create unsigned extension:
1.
    mkdir example
    cd example/
    vi ciao.txt
    mkdir usr
    cp ciao.txt usr/ciao2.txt
    ls usr/
    source /etc/os-release
    mkdir -p usr/lib/extension-release.d/
    echo ID=$ID > usr/lib/extension-release.d/extension-release.example
    echo VERSION_ID=$VERSION_ID >> usr/lib/extension-release.d/extension-release.example
    echo SYSEXT_SCOPE=initrd >> usr/lib/extension-release.d/extension-release.example
    cat usr/lib/extension-release.d/extension-release.example
    cd ..
    mksquashfs example example.raw
2. copy extension in EFI
    mv example.raw /boot/efi/EFI/Linux/devel.efi.extra.d/
3. Since this is an unsigned extension, add rd.break into the UKI cmdline as addon
4. reboot
5. Enter the initrd console and run
SYSTEMD_LOG_LEVEL=debug SYSTEMD_DISSECT_VERITY_SIGNATURE=0 systemd-sysext merge --image-policy "root=unprotected+absent:usr=unprotected+absent"
6. check in the initrd console that the file exist
7. continue with ctrl+d

Create a signed extension:
./mkosi_sysext_script.sh
cd /tmp/mkosi/mkosi.output
ls btrfs.raw # <--- image
cd ..
mkdir -p /usr/lib/verity.d # alternative temp are {/etc, /run}/verity.d
cp -r /tmp/mkosi ~
sudo cp ~/mkosi/mkosi.crt /usr/lib/verity.d/mkosi.crt
systemd-dissect --mtree mkosi.output/btrfs.raw # inspect image

## inspect initrd to see if module is present
mkdir /tmp/initrd
cd /tmp/initrd
objcopy -O binary --only-section=".initrd" /boot/efi/EFI/Linux/devel.efi devel-extr
unxz -c  devel-extr | cpio -idmv

## inspect systemd-stub logs (useless) by accessing EDK
-chardev stdio,id=seabios -device isa-debugcon,iobase=0x402,chardev=seabios

systemctl status systemd-sysext --no-pager -l

dracut --list-modules # shows available dracut modules

inspect current initrd:
1. add rd.break cmdline addon to UKI and reboot
3. systemd-sysext list
4. systemd-dissect --list


Required kernel config:
Required for signed Verity images support:
	CONFIG_DM_VERITY_VERIFY_ROOTHASH_SIG
Required to verify signed Verity images using keys enrolled in the MoK
(Machine-Owner Key) keyring:
	CONFIG_DM_VERITY_VERIFY_ROOTHASH_SIG_SECONDARY_KEYRING
	CONFIG_IMA_ARCH_POLICY
	CONFIG_INTEGRITY_MACHINE_KEYRING


Install custom shim:
1. Add repo: https://copr.fedorainfracloud.org/coprs/kraxel/shim.playground/repo/fedora-rawhide/kraxel-shim.playground-fedora-rawhide.repo
2. sudo dnf update shim-x64
3. certutil -L -d /etc/pki/pesign-rh-test -n "Red Hat Test CA" -a \
	| openssl x509 -text > secureboot.rhtest.ca.pem
4. virt-fw-vars \
        --input fedora_old/OVMF_VARS.secboot.fd \ # --set-shim-verbose
        --output fedora/OVMF_VARS.secboot.fd \
        --enroll-redhat \
        --distro fedora \
        --distro centos \
        --add-db OvmfEnrollDefaultKeys secureboot.rhtest.ca.pem \
        --secure-boot


virt-fw-vars -i ~/kernel/vms/vars/fedora_old/OVMF_VARS.secboot.fd --output-json test
Edit var to make mok work
{
    "version": 2,
    "variables": [
        {
            "name": "MokListTrusted",
            "guid": "605dab50-e046-4300-abb6-3dd810dd8b23",
            "attr": 2,
            "data": "01"
        }
    ]
}
virt-fw-vars -i ~/kernel/vms/vars/fedora_old/OVMF_VARS.secboot.fd -o ~/kernel/vms/vars/fedora/OVMF_VARS.secboot.fd --set-json test
virt-fw-vars -i ~/kernel/vms/vars/fedora/OVMF_VARS.secboot.fd --output-json test

check vars
ls /sys/firmware/efi/efivars/ | grep Mok

xxd /sys/firmware/efi/mok-variables/MokListTrustedRT


########################################### Kernel shrink

mount -t overlay overlay -o lowerdir=sysext:test,upperdir=out,workdir=workdir merged

## how the process works: ##

host:
- dracut module that contains
    * copy script (prepares sysext + mv files)
    * systemd unit (calls copy script before switching to root)

initrd:
- systemd service
    calls the copy script that creates the extension in /run/extensions and copy kernel
    Also starts overlay kmod

rootfs:
- systemd-sysext service
    reads the extension in /run/extension and mounts it, so that all kmods are available.

## debug ##

depmod -b /run/extensions/modules_ext/usr

modules in initramfs are in usr/lib/modules/6.9.0-0.rc0.20240322git8e938e398669.14.fc41.x86_64/kernel

modules in kernel are in /lib/modules/6.9.0-0.rc0.20240322git8e938e398669.14.fc41.x86_64/kernel/drivers

rpm -e --nodeps kernel-core kernel-modules-code kernel kernel-core-modules # get rid of the existing modules

dracutmodules+=" systemd-sysext systemd-sysext-extensions "
source /mnt/eesposit-scripts/qemu_machine/uki/load_efi_Fedora_vars.sh
efid

insmod /run/extensions/modules_ext/usr/lib/modules/6.9.0-0.rc2.23.fc41.x86_64/kernel/fs/overlayfs/overlay.ko.xz

# when doing the install manually, if the modules need a depmod do ctrl+d (logout) to make all systemd services restart

# to install latest dracut, do make && make install. if it complains about the
# read only fs, systemctl stop systemd-sysext and then later do start


######## KERNEL-ARCH

/var/lib/mock/fedor/var/lib/mock/fedora-39-x86_64/result/kernel-uki-virt-6.8.0-0.rc0.052d534373b7.7.test.fc39.x86_64.rpm

# create srpm
make -j $(getconf _NPROCESSORS_ONLN) dist-srpm

# compile it with mock (any system, theoretically)
mock -r /etc/mock/fedora-39-x86_64.cfg redhat/rpm/SRPMS/kernel-6.9.0-0.rc0.052d534373b7.0.test.fc39.src.rpm

# compile it locally
rpmbuild --rebuild /path/to/kernel/srpm

##################################### kmod

./autogen.sh
./configure CFLAGS="-g -O2" --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib --with-xz --with-openssl --with-zstd --with-zlib
make && ./tools/depmod

cd /usr/sbin
sudo mkdir original
sudo mv depmod original/
sudo mv insmod original/
sudo mv rmmod original/
sudo mv lsmod original/
sudo mv modinfo original/
sudo mv modprobe original/
cd /usr/bin/
sudo mkdir original
sudo mv kmod original


cd /usr/sbin
sudo mkdir custom
sudo mv depmod custom/
sudo mv insmod custom/
sudo mv rmmod custom/
sudo mv lsmod custom/
sudo mv modinfo custom/
sudo mv modprobe custom/
cd /usr/bin/
sudo mkdir custom
sudo mv kmod custom
cd /usr/sbin
sudo cp original/depmod depmod
sudo cp original/insmod insmod
sudo cp original/rmmod rmmod
sudo cp original/lsmod lsmod
sudo cp original/modinfo modinfo
sudo cp original/modprobe modprobe
cd /usr/bin/
sudo cp original/kmod kmod



########################### disk verity
# 1. create RHEL using rhel9-azure-cvm.ks. This ks has /verity-root partition,
# plus formats partitions using ext4 and block size 4096.
# Instructions for this are at the beginning of the file
virt-install --virt-type kvm --os-variant rhel9.0 --arch x86_64 --boot uefi --name rhel9.5-uki-verity --memory 8192 --location ~/.local/share/libvirt/images/isos/RHEL-9.5.0-20240616.55-x86_64-dvd1.iso --disk bus=scsi,size=30 --initrd-inject=rhel9-azure-cvm.ks --nographics --extra-args "console=ttyS0 inst.ks=file:/rhel9-azure-cvm.ks"
# 2. Once booted, do fdisk to change partition type to Linux root (used by systemd-gpt-auto),
# install uki and all packages. Instructions on top of the file
# 3. install custom kernel-uki-virt, or create it yourself
https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=62108036
# 4. disable secureboot
sudo mokutil --disable-validation
create pw
reboot
follow mokutil menu
# 5 create UKI addon for overlayfs
sudo /usr/lib/systemd/ukify build --cmdline="rd.live.overlay.overlayfs=1" --output=overlay.addon.efi
# 5. remove /verity-root from /etc/fstab, otherwise systemd tries to load it at boot
REMOVE VERITY PARTITION FROM THE /etc/fstab, use debug_efi to do it
# 6. create symlink for network manager
mkdir -p /var/etc//NetworkManager/
ln -sf /etc/NetworkManager/system-connections /var/etc/NetworkManager/system-connections
# 7. shut down, and run verity.sh to verity-setup the /verity-root. Requires veritysetup
    # hash the root
    sudo modprobe nbd
    sudo qemu-nbd -c /dev/nbd0 -f qcow2 /home/esposem/.local/share/libvirt/images/fedora-CVM.qcow2
    # p3 is where the root disk in this case is (Linux root filesystem)
    sudo veritysetup format /dev/nbd0p3 /dev/nbd0p4
    Then copy the "Root hash:"
    echo "5ad336950f2567dfc26f1351d741ba4bfead4b090e184e476d1760bb6b610a6e" > roothash.txt

    # verify it works: should produce no output
    sudo veritysetup verify /dev/nbd0p3 /dev/nbd0p4 --root-hash-file=roothash.txt

    # prepare addon. In this case, EFI is p2
    #----- Doing it from the host, doesn't work bc of ukify being old
    sudo mount /dev/nbd0p2 /mnt
    cd /mnt
    cd EFI/Linux/15468706b9a2426e8d4b97649a8f89eb-6.9.4-200.fc40.x86_64.efi.extra.d
    ukify build --cmdline='systemd.verity=1 roothash=5ad336950f2567dfc26f1351d741ba4bfead4b090e184e476d1760bb6b610a6e systemd.verity_root_data=/dev/vda3 systemd.verity_root_hash=/dev/vda4' --output=verity.addon.efi
    cd -
    sudo umount /mnt

    # CHECK: enter the fs
    sudo veritysetup open /dev/nbd0p3 testfs /dev/nbd0p4 $(cat roothash.txt)
    sudo mount /dev/mapper/testfs mnt
    # inspect
    sudo umount mnt

    # Close all
    sudo veritysetup close testfs
    sudo qemu-nbd --disconnect /dev/nbd0

# DEBUG: if no internet
nmcli con up enp1s0

# DEBUG: if sulogin: crypt failed: Invalid argument in initramfs when logging with rd.break,
# disable SELINUX
selinux=0






printf "shimx64.efi,sdboot,\\\EFI\\\systemd\\\systemd-bootx64.efi ,SDBOOT\n" | iconv -f ASCII -t UCS-2 > tmp/efi/EFI/systemd/BOOTX64.CSV
printf "shimx64.efi,systemd,\\\EFI\\\Linux\\\242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi ,UKI bootentry\n" | iconv -f ASCII -t UCS-2 > tmp/efi/EFI/systemd/BOOTX64.CSV


printf "shimx64.efi,sdboot,\\\EFI\\\systemd\\\systemd-bootx64.efi ,SDBOOT\n" | iconv -f ASCII -t UCS-2 > qmnt/EFI/systemd/BOOTX64.CSV
printf "shimx64.efi,systemd,\\\EFI\\\Linux\\\242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi ,UKI bootentry\n" | iconv -f ASCII -t UCS-2 > qmnt/EFI/systemd/BOOTX64.CSV

sudo cp /boot/efi/EFI/fedora/shimx64.efi qmnt/EFI/BOOT/BOOTX64.EFI
sudo cp /boot/efi/EFI/BOOT/fbx64.efi qmnt/EFI/BOOT/fbx64.efi
sudo cp /boot/efi/EFI/fedora/mmx64.efi qmnt/EFI/systemd
sudo cp /boot/efi/EFI/fedora/shimx64.efi qmnt/EFI/systemd
sudo cp /boot/efi/EFI/fedora/shim.efi qmnt/EFI/systemd

sudo rm -rf qmnt/EFI/BOOT/BOOTX64.EFI
sudo rm -rf qmnt/EFI/BOOT/fbx64.efi
sudo rm -rf qmnt/EFI/systemd/*


------
composefs
	images : symlink to the image, pointing to a specific obj in objects
	objects: all files. They are indexed by fsverity hash
	streams: ??

#try to mess with composefs:
sudo composefs-info dump images/078570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc28 | grep etc/host
/etc/hosts 384 100644 1 0 0 0 1735257600.0 b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff - b745186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff security.selinux=system_u:object_r:net_conf_t:s0

--> b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff

Try to change that file, or rm, or add another one

# try to modify fs-verity enabled file
echo "asd" > b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff
-bash: b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff: Operation not permitted

# Create new file
touch b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36f9 # fake file
echo "asd" > b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36f9 # OK
fsverity enable b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36f9 # now it cannot be modified anymore
boot
# works???
# file not found anywhere

# delete file
rm b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff
boot
# reading the file fails

# replace with custom file
rm b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff
echo "asd" > b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff
fsverity enable b7/45186e892c8276695a2a4af80213e283e4e126727610a7535d1980172e36ff
boot # boots fine
# reading the file fails

# create a more flexible uki with no composefs= hardcoded, so we can play with it
objcopy -O binary --only-section=".linux" 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi vmlinux
objcopy -O binary --only-section=".initrd" 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi initrd

ukify build --linux=vmlinux --initrd=initrd --cmdline="rw systemd.machine_id=242c3e59532d417e82907fec335a47b2" --output=242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0
abee6.11.fc42.x86_64.efi2 # cmdline copied from original uki

mkdir 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi.extra.d
cd 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi.extra.d
# build fake composefs
ukify build --cmdline="composefs=078570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc30" --output=composefs.addon.efi # made up
ukify build --cmdline="composefs=078570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc28" --output=composefs.addon.efi # correct
# replace uki
mv 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi.orig
mv 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi2 242c3e59532d417e82907fec335a47b2-6.14.0-0.rc0.20250130git72deda0abee6.11.fc42.x86_64.efi
# fails in initrd because the composefs= is the image fsverity digest which is also the path into composefs/images to find it! So there must be a symlink in images/ called ...30 poiting to ...30

# what if I change images/ symlink to a malicious image but the name is pointing to the correct hash?
# was:
images/...28 -> objects/...28 # digest is 28, image is 28
# changed it in
images/...30 -> objects/...28 # now assume correct digest is 30, but point it to 28 (my malicious image)
ln -s objects/07/8570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc28 images/078570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc30
Feb 05 09:23:59 fedora composefs-pivot-sysroot[490]: Error: Expected digest 078570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc30 but found 078570aff8bc6ef542b99f9881e1c05e320b5344d1dc78e883cd2df5cd8cdc28
#fails to boot!





TODO if it flies
- kmod
	tests
	make /run/modules as macro

- dracut
	make sure it creates a initramfs correctly (mods in /lib/modules)
	but accepts the input mods into /run/modules
	makes sure it doesn't copy if /sysroot is not empty


Changes:
- kernel-ark:
	get rid of dependency for kernel-modules-core
	modify dracut-virt.conf to have custom module and add drivers you want

- dracut:
	install the custom module

- kmod:
	look into /run/modules first

modules.builtin.bin modules.order modules.alias modules.builtin.modinfo modules.softdep modules.alias.bin modules.dep modules.symbols modules.builtin modules.dep.bin modules.symbols.bin modules.builtin.alias.bin modules.devname



sudo dnf install kernel-modules kernel-devel
rm -rf ~/kmod && mkdir ~/kmod
meson setup -Denable-alternative-dir=true ~/kmod/
meson setup -Dbuild-tests=true -Denable-alternative-dir=true ~/kmod
meson compile -C ~/kmod

modprobe non va perche era crit forse prima l errore?
TODO: complie && make install && reboot