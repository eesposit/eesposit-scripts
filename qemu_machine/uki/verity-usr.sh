#! /bin/bash
# set -x

if [ -z ${QCOW2+x} ]; then
    echo "QCOW2 is unset";
    exit
else
    echo "QCOW2 $QCOW2"
fi


VERITY_FOLDER=$(mktemp -d)
# RHEL setup
EFI_PN=${EFI_PN:-"1"}
ROOT_PN=${ROOT_PN:-"2"}
USR_PN=${USR_PN:-"3"}
UKI_NAME=${UKI_NAME:-"devel.efi"}

echo VERITY_FOLDER=$VERITY_FOLDER
echo EFI_PN=$EFI_PN
echo ROOT_PN=$ROOT_PN
echo USR_PN=$USR_PN
echo UKI_NAME=$UKI_NAME

here=`pwd`

dir_created=0
nbd_mounted=0

function handle_ctrlc() {
    # if [[ $dir_created == 1 ]]; then
    #     cd $VERITY_FOLDER
    #     cd ..
    #     rm -rf $VERITY_FOLDER
    # fi
    if [[ $nbd_mounted == 1 ]]; then
        sudo qemu-nbd --disconnect /dev/nbd0
    fi
    cd $here
    exit
}

# trapping the SIGINT signal
trap handle_ctrlc SIGINT
trap handle_ctrlc EXIT

cd $VERITY_FOLDER
dir_created=1

mkdir mnt
mkdir conf
mkdir mnt_usr

echo "[Partition]
Type=usr-verity
Verity=hash
VerityMatchKey=asd
SizeMinBytes=500M
SizeMaxBytes=500M" > conf/verity.conf

echo "[Partition]
Type=usr
Verity=data
VerityMatchKey=asd
SizeMinBytes=5G
SizeMaxBytes=5G" > conf/usr.conf

sudo modprobe nbd
sudo qemu-nbd -c /dev/nbd0 -f qcow2 $QCOW2
nbd_mounted=1


# echo "Copying /etc in /usr partition..."
# sudo mount /dev/nbd0p$USR_PN mnt_usr
# sudo mount /dev/nbd0p$ROOT_PN mnt

# # ensure usr (/usr) is mounted ro in /etc/fstab, otherwise systemd-remount-fs.service fails
# sudo sed -i '/[[:space:]]\/usr[[:space:]]/ s/\bdefaults\b/ro,defaults/' mnt/etc/fstab

# sudo cp -a mnt/etc mnt_usr

## echo "d /var/lib/gssproxy 755 root root -
## A /var/lib/gssproxy - - - - system_u:object_r:gssproxy_var_lib_t:s0
## d /var/lib/logrotate 755 root root -
## A /var/lib/logrotate - - - - system_u:object_r:logrotate_var_lib_t:s0" > verity.conf
## sudo cp verity.conf mnt_usr/lib/tmpfiles.d

# sudo umount mnt
# sudo umount mnt_usr
# echo "Copying /etc in /usr partition OK"

RH="TBD"

function repart() {
    # sudo mount /dev/nbd0p$ROOT_PN mnt
    sudo systemd-repart /dev/nbd0 --dry-run=yes --definitions=$VERITY_FOLDER/conf --no-pager
    sudo systemd-repart /dev/nbd0 --dry-run=no --definitions=$VERITY_FOLDER/conf --no-pager --json=pretty | jq -r '.[] | select(.type == "usr-x86-64-verity") | .roothash' > usrhash.txt
    RH=$(cat usrhash.txt)
    echo "Roothash: $RH"
    if [[ $RH = "TBD" ]]; then
        echo "Partition already exists, first destroy";
        # sudo umount mnt
        sudo sfdisk --delete /dev/nbd0 4 # TODO: this is hardcoded
        repart
        return 0
    fi
    # sudo umount mnt
}

# create /usr-verity partition
echo "Creating /usr verity partition..."
repart
echo "Creating /usr verity partition OK"

### TODO: this is done into kickstart
# sudo sfdisk /dev/nbd0 --list
# sudo sfdisk --delete /dev/nbd0 2
# sudo sfdisk /dev/nbd0 --list
#####

# prepare addon. EFI is EFI_PN
echo "Creating addon into /boot/efi..."
sudo mount /dev/nbd0p$EFI_PN mnt
sudo mkdir -p mnt/EFI/Linux/${UKI_NAME}.extra.d
cd mnt/EFI/Linux/${UKI_NAME}.extra.d
sudo rm -f verity.addon.efi
sudo /usr/lib/systemd/ukify build --cmdline="usrhash=$RH" --output=verity.addon.efi
sudo /usr/lib/systemd/ukify inspect verity.addon.efi
sudo fdisk /dev/nbd0 --list
cd - && sudo umount mnt
echo "Creating addon into /boot/efi OK"

sudo qemu-nbd --disconnect /dev/nbd0
# cd $VERITY_FOLDER
# cd ..
# rm -rf $VERITY_FOLDER
cd $here