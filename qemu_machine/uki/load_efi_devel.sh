#!/bin/bash
set -e

# Customizable fields (default are for rhel):
EFI_FILE=${EFI_FILE:-"devel.efi"}
EFI_NAME=${EFI_NAME:-"UKI_devel"}
KERNEL_VERSION=${KERNEL_VERSION:-"5.14.0-536.el9.x86_64"}
KERNEL_LOCATION=${KERNEL_LOCATION:-"/boot/vmlinuz-$KERNEL_VERSION"}
SYSTEMD_STUB_LOCATION=${SYSTEMD_STUB_LOCATION:-"/usr/lib/systemd/boot/efi/linuxx64.efi.stub"}
INITRAMFS_LOCATION=${INITRAMFS_LOCATION:-"/boot/initramfs-$KERNEL_VERSION.img"}
OUTPUT_EFI=${OUTPUT_EFI:-"/boot/efi/EFI/Linux/$EFI_FILE"}
CD=${CD:-"Signed UKI"}
TMP_OUT=${TMP_OUT:-"/tmp/uki"}
UKIFY=${UKIFY:-"/usr/bin/ukify"}
SIGN=${SIGN:-false}

FILE_POS=`dirname $0`
DRACUT_VIRT=${DRACUT_VIRT:-"$FILE_POS/dracut-virt.conf"}

# DEF_CMD_LINE="console=tty0 console=ttyS0 quiet loglevel=3 systemd.show_status=auto"
DEF_CMD_LINE="console=tty0 console=ttyS0"
CMD_LINE=`cat /mnt/cmdline.txt 2> /dev/null || echo $DEF_CMD_LINE`

DRACUT_BIN=${DRACUT_BIN:-"dracut"}

echo "EFI FILE NAME $EFI_FILE"
echo "EFI LABEL NAME $EFI_NAME"
echo "EFI CMD_LINE $CMD_LINE"
echo "KERNEL $KERNEL_VERSION"
echo "OUTPUT $OUTPUT_EFI"
echo "CERT $CD"
echo "DRACUT_BIN $DRACUT_BIN"
echo "DRACUT $DRACUT_VIRT"
echo "UKIFY $UKIFY"

echo "---------------------------------------"

### Manual build (have to figure the right addresses)
# Use custom initramfs since addres offset is calculated based on the new generated one.
# See/use https://github.com/esposem/dracut/commit/53960ffa091f7125f204f284463f0742ce683c9e
# INITRAMFS_LOCATION="/root/additional_section/dracut_generated/initramfs.img"
# echo "####### OBJCOPY #######"
# objcopy \
# --add-section .osrel=/etc/os-release --change-section-vma .osrel=0x14dfaf000 \
# --add-section .cmdline=/mnt/cmdline.txt --change-section-vma .cmdline=0x14dfb0000 \
# --add-section .linux=$KERNEL_LOCATION --change-section-vma .linux=0x14dfb1000 \
# --add-section .initrd=$INITRAMFS_LOCATION --change-section-vma .initrd=0x14eb4e000 \
# --add-section .cmdlist=/mnt/allowlist.txt --change-section-vma .cmdlist=0x1504fa000 \
# $SYSTEMD_STUB_LOCATION \
# $TMP_OUT

### Dracut build (RH default)
# Dracut generates its own initramfs, so no need to provide it in input.
# echo "####### DRACUT #######"
# $DRACUT_BIN --conf=$DRACUT_VIRT --confdir=$(mktemp -d) --kver $KERNEL_VERSION \
#  --kmoddir=/lib/modules/$KERNEL_VERSION/ --uefi --kernel-image=$KERNEL_LOCATION \
#  --kernel-cmdline "$CMD_LINE" --uefi-stub $SYSTEMD_STUB_LOCATION  --force $TMP_OUT # --logfile=log --verbose

CUSTOM_INITRAMFS="./initramfs-custom.img"
echo "####### INITRAMFS #######"
rm -rf $CUSTOM_INITRAMFS
$DRACUT_BIN --conf=$DRACUT_VIRT --confdir=$(mktemp -d) --kver $KERNEL_VERSION --kmoddir=/lib/modules/$KERNEL_VERSION/ --force $CUSTOM_INITRAMFS --verbose
INITRAMFS_LOCATION=$CUSTOM_INITRAMFS

echo "INITRAMFS_LOCATION $INITRAMFS_LOCATION"
echo "---------------------------------------"

### Ukify build (systemd alternative)
echo "####### UKIFY #######"
$UKIFY build --linux=$KERNEL_LOCATION --initrd=$INITRAMFS_LOCATION --uname $(uname -r) \
--cmdline "$CMD_LINE" --stub $SYSTEMD_STUB_LOCATION -o $TMP_OUT

# printf "\\\EFI\\\Linux\\\\${EFI_FILE}\0" | iconv -f ASCII -t UCS-2 > /tmp/bootarg
# efibootmgr -c -d /dev/vda -p $PART --append-binary-args /tmp/bootarg -L $EFI_NAME \
#  -l "\EFI\\${OS}\shimx64.efi"

rm -f $OUTPUT_EFI

if $SIGN; then
	echo "---------------------------------------"
	echo "####### PESIGN #######"

	pesign -c $CD -i $TMP_OUT --force -s -o $OUTPUT_EFI
	rm $TMP_OUT

	pesign -S -i $OUTPUT_EFI
	echo "---------------------------------------"

else
	echo "---------------------------------------"
	echo "####### MV #######"

	mv $TMP_OUT $OUTPUT_EFI
	echo "---------------------------------------"
fi

echo "####### ADD ENTRY #######"
kernel-bootcfg --add-uki $OUTPUT_EFI --title $EFI_NAME --boot-order 0


rm -rf $CUSTOM_INITRAMFS
