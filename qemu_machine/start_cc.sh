#!/bin/bash

QCOW2=qcow2/rhel-guest-image-9.3-20230313.0.x86_64.qcow2

cd ~/kernel/vms

sudo ./virtiofsd --socket-path=sock/vm001-vhost-fs.sock  -o source=./shared_host_guest/ &

SECBOOT=.secboot
#SECBOOT=""

sudo /usr/libexec/qemu-kvm \
-name azure-cvm \
-smp 28 \
-machine q35,smm=on,accel=kvm,kernel-irqchip=split \
-global driver=cfi.pflash01,property=secure,value=on \
-cpu host -drive id=drive_image2,if=none,snapshot=off,aio=threads,cache=none,format=qcow2,file=$QCOW2 \
-drive file=/usr/share/edk2/ovmf/OVMF_CODE$SECBOOT.fd,if=pflash,format=raw,readonly=on,unit=0 \
-drive file=vars/rhel/OVMF_VARS$SECBOOT.fd,if=pflash,format=raw,unit=1 \
-device virtio-blk-pci,id=image2,drive=drive_image2,bootindex=3,serial=SYSTEM_DISK1,bus=pcie.0,addr=0x8 \
-device ahci,id=ahci0 \
-m 32G \
-boot menu=on \
-nic bridge,br=virbr0,helper=/usr/libexec/qemu-bridge-helper \
-chardev socket,id=char0,path=sock/vm001-vhost-fs.sock \
-device vhost-user-fs-pci,chardev=char0,tag=myfs \
-object memory-backend-memfd,id=mem,size=32G,share=on \
-numa node,memdev=mem \
-net user,hostfwd=tcp::10022-:22 \
-net nic \
-smbios type=11,value=io.systemd.stub.kernel-cmdline-extra=NONSAFE \
--nographic

# -nodefaults -serial file:qemu_log
# -debugcon file:debug.log -global isa-debugcon.iobase=0x402 

# -chardev socket,id=chrtpm,path=/root/azure_cvm/tmp2.sock \
# -tpmdev emulator,id=tpm0,chardev=chrtpm -device tpm-tis,tpmdev=tpm0
