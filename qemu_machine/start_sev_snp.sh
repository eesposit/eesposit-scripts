swtpm socket --tpmstate dir=tpm \
    --ctrl type=unixio,path=tpm/swtpm-sock \
    --log level=40 --tpm2 -t -d

/usr/libexec/qemu-kvm \
-name azure-cvm \
-smp 28 \
-machine q35,confidential-guest-support=sev0,accel=kvm,kernel-irqchip=split \
-object sev-snp-guest,id=sev0,cbitpos=51,reduced-phys-bits=1,id-auth= \
-bios /usr/share/edk2/ovmf/OVMF.amdsev.fd \
-global driver=cfi.pflash01,property=secure,value=on \
-cpu host -drive id=drive_image2,if=none,snapshot=off,aio=threads,cache=none,format=qcow2,file=RHEL-9.6.0-20250112.2-x86_64-ovmf-snp.qcow2 \
-device virtio-blk-pci,id=image2,drive=drive_image2,bootindex=3,serial=SYSTEM_DISK1,bus=pcie.0,addr=0x8 \
-device ahci,id=ahci0 \
-m 32G \
-boot menu=on \
-nic bridge,br=virbr0,helper=/usr/libexec/qemu-bridge-helper \
-net user,hostfwd=tcp::10022-:22 \
-net nic \
-smbios type=11,value=io.systemd.stub.kernel-cmdline-extra=NONSAFE \
-chardev socket,id=chrtpm,path=tpm/swtpm-sock \
-tpmdev emulator,id=tpm1,chardev=chrtpm -device tpm-tis,tpmdev=tpm1 \
--nographic