#! /bin/bash

# HOW TO RUN:
# 1. download virtiofsd exec
# 2. run the alias vsfds
# 3. run this script
# 4. in guest, run `mount -t virtiofs myfs /mnt`
# 5. shared files are in /mnt

QCOW2=/home/eesposit/.local/share/libvirt/images/rhel9.6-dm.qcow2
VARS=rhel9.6

SECBOOT=.secboot
# SECBOOT=""

# sudo dnf install swtpm swtpm-tools

cd ~/kernel/vms

# swtpm_setup --tpm2 --tpm-state ~/kernel/vms/sock/tpm1
swtpm socket --tpmstate dir=sock/tpm1 \
    --ctrl type=unixio,path=sock/tpm1/swtpm-sock \
    --log level=40 --tpm2 -t -d

sudo ./virtiofsd --socket-path=sock/vm002-vhost-fs.sock  -o source=./shared_host_guest/ &

sudo /usr/libexec/qemu-kvm \
-name azure-cvm \
-smp 28 \
-machine q35,smm=on,accel=kvm,kernel-irqchip=split \
-global driver=cfi.pflash01,property=secure,value=on \
-cpu host -drive id=drive_image2,if=none,snapshot=off,aio=threads,cache=none,format=qcow2,file=$QCOW2 \
-drive file=/usr/share/edk2/ovmf/OVMF_CODE$SECBOOT.fd,if=pflash,format=raw,readonly=on,unit=0 \
-drive file=vars/$VARS/OVMF_VARS$SECBOOT.fd,if=pflash,format=raw,unit=1 \
-device virtio-blk-pci,id=image2,drive=drive_image2,bootindex=3,serial=SYSTEM_DISK1,bus=pcie.0,addr=0x8 \
-device ahci,id=ahci0 \
-m 32G \
-boot menu=on \
-nic bridge,br=virbr0,helper=/usr/libexec/qemu-bridge-helper \
-chardev socket,id=char0,path=sock/vm002-vhost-fs.sock \
-device vhost-user-fs-pci,chardev=char0,tag=myfs \
-object memory-backend-memfd,id=mem,size=32G,share=on \
-numa node,memdev=mem \
-net user,hostfwd=tcp::10022-:22 \
-net nic \
-smbios type=11,value=io.systemd.stub.kernel-cmdline-extra=NONSAFE \
-chardev socket,id=chrtpm,path=sock/tpm1/swtpm-sock \
-tpmdev emulator,id=tpm1,chardev=chrtpm -device tpm-tis,tpmdev=tpm1 \
--nographic

# -serial chardev:char1
# -chardev stdio,id=char1,mux=on,logfile=serial.log,signal=off
# -monitor none -vnc none

# -chardev socket,id=chrtpm,path=sock/tpm1/swtpm-sock
# -tpmdev emulator,id=tpm1,chardev=chrtpm -device tpm-tis,tpmdev=tpm1
# -nodefaults -serial file:qemu_log
# -debugcon file:debug.log -global isa-debugcon.iobase=0x402
