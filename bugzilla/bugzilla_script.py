import bugzilla
import os
import argparse
import pprint
import xmlrpc
import datetime
from datetime import date

old = {
	"2022-07-04" : 18,
	"2022-07-11" : 19,
	"2022-07-18" : 20,
	"2022-07-25" : 21,
	"2022-08-01" : 22,
	"2022-08-08" : 23,
	"2022-08-15" : 24,
	"2022-08-22" : 25,
	"2022-08-29" : 26,
	"2022-09-05" : 27,
	"2022-09-12" : 28,
	"2022-09-19" : 29,
	"2022-09-26" : 30,
	"2022-10-03" : 31,
	"2022-10-10" : 32,
	"2022-10-17" : 33,
	"2022-10-24" : 34,
	"2022-10-31" : 35,
	"2022-11-07" : 36,
}

new = {
	"2022-09-05" : 1,
	"2022-09-12" : 2,
	"2022-09-19" : 3,
	"2022-09-26" : 4,
	"2022-10-03" : 5,
	"2022-10-10" : 6,
	"2022-10-17" : 7,
	"2022-10-24" : 8,
	"2022-10-31" : 9,
	"2022-11-07" : 10,
	"2022-11-14" : 11,
	"2022-11-21" : 12,
	"2022-11-28" : 13,
	"2022-12-05" : 14,
	"2022-12-12" : 15,
	"2022-12-19" : 16,
	"2022-12-26" : 17,
	"2023-01-02" : 18,
	"2023-01-09" : 19,
	"2023-01-16" : 20,
	"2023-01-23" : 21,
	"2023-01-30" : 22,
	"2023-02-06" : 23,
	"2023-02-13" : 24,
	"2023-02-20" : 25,
	"2023-02-27" : 26,
	"2023-03-06" : 27,
	"2023-03-13" : 28,
	"2023-03-20" : 29,
	"2023-03-27" : 30,
	"2023-04-03" : 31,
	"2023-04-10" : 32,
	"2023-04-17" : 33,
	"2023-04-24" : 34,
	"2023-05-01" : 35,
	"2023-05-08" : 36,
}

DTMS = {
	"8.7": old,
	"9.1": old,
	"8.8": new,
	"9.2": new,
}

enable_debug = False

def print_debug(string):
    if enable_debug:
        print(string)

URL='https://bugzilla.redhat.com/xmlrpc.cgi'
DEFAULT_PATH="~/.config/python-bugzilla/bugzillarc"

needinfo_setter = 'eesposit'

def ask_needinfo(requestee, obj):
    flag = {
        'requestee' : requestee,
        'name' : 'needinfo',
        'type_id': 16,
        'status' : '?',
        'new' : True,
    }

    if not "flags" in obj:
        obj["flags"] = []

    obj["flags"].append(flag)

def handle_bz(bz_number, mr, version):
    bug = bzapi.getbug(bz_number)
    modifications = {}
    bug.refresh()
    print("\tFetched bug #%s" % bug.id)

    if not (bug.status == 'NEW' or bug.status == 'ASSIGNED'):
        print("#### Bug %s is not NEW or ASSIGNED, but %s!" % (bug.id, bug.status))
        # exit(1)

    bug_itr = bug.cf_internal_target_release
    if bug_itr == '---':
        bug_itr = None
    print("ITR is %s" % bug_itr)

    try:
        bug_dtm = bug.cf_dev_target_milestone
    except AttributeError:
        bug_dtm = None
    print("DTM is %s" % bug_dtm)

    try:
        bug_itm = bug.cf_internal_target_milestone
    except AttributeError:
        bug_itm = None
    print("ITM is %s" % bug_itm)

    try:
        bug_zstream = bug.cf_zstream_target_release
    except AttributeError:
        bug_zstream = None
    print("ZTR is %s" % bug_zstream)

    version_str = version + ".0"
    if bug_zstream != None and bug_itr != None and version_str != bug_zstream and version_str != bug_itr:
        print("#### Bug %s has ITR %s and ZTR %s different from branch %s" % (bug.id, bug_itr, bug_zstream, version_str))
        exit(1)

    devel_ack_flag = bug.get_flag_type("devel_ack")
    print_debug("devel_ack is %s" % devel_ack_flag)
    qa_ack_flag = bug.get_flag_type("qa_ack")
    print_debug("qa_ack is %s" % qa_ack_flag)
    zstream_flag = bug.get_flag_type("zstream")
    print_debug("zstream is %s" % zstream_flag)

    comment_str = ""

    # it's a z-stream: all done manually (ITR, ZTR, DTM)
    # zstream_flag.get("status") == '+'
    if not zstream_flag:
        # not a z-stream: update dtm and itr
        if bug_itr is None:
            print("\t\tSetting ITR to %s" % version_str)
            modifications["cf_internal_target_release"] = version_str
        if bug_dtm is None:
            today = date.today()
            target_date = str(today + datetime.timedelta(days=-today.weekday(), weeks=2))
            print("\t\tSUGGESTED DTM date: %s " % target_date)
            # if target_date in DTMS[version]:
            #     dtm = DTMS[version][target_date]
            #     print("\t\tSetting DTM to %s" % dtm)
            #     modifications["cf_dev_target_milestone"] = dtm
            # else:
            #     print("\t\tERROR Setting DTM: no DTM available for %s" % target_date)

        qa_contact = bug.qa_contact
        print("qa contact is %s" % qa_contact)

        default = False
        for el in ["rhel", "virt", "qe", "qa", "bugs"]:
            if el in qa_contact:
                default = True

        if not default:
            maybe_needinfo = "@%s, can you please set " % qa_contact
            el_set = False

            if bug_itm is None:
                maybe_needinfo += "ITM"
                el_set = True

            if qa_ack_flag and qa_ack_flag['status'] != '+':
                if el_set:
                    maybe_needinfo += " and "
                maybe_needinfo += "qa_ack+"
                el_set = True

            maybe_needinfo += "?\n"
            if el_set:
                comment_str += maybe_needinfo
                ask_needinfo(qa_contact, modifications)


    if bug.status == 'NEW' or bug.status  == 'ASSIGNED':
        comment_str += "MR sent: %s\nMoving to POST\n" % mr
        modifications["status"] = 'POST'


    if comment_str != "":
        print("\nComments are:")
        comment_str += "\nThank you,\nEmanuele"
        pprint.pprint(comment_str)

        confirm = input("Do you want send comments?: ")

        if confirm[0] == 'y':
            print("Updated!")
            bug.addcomment(comment_str, private=True)
        else:
            print("Comments not sent!")
    else:
        print("No comment to send!")


    if modifications:
        print("\n\nModifications are:")
        pprint.pprint(modifications)

        confirm = input("Do you want to send modifications?: ")

        if confirm[0] == 'y':
            print("Updated!")
            bzapi.update_bugs([bz_number], modifications)
        else:
            print("Modifications not sent!")
    else:
        print("No modification to send!")


if __name__=="__main__":
    path = os.path.expanduser(DEFAULT_PATH)
    api_key = ""
    with open(path) as f:
        for line in f.readlines():
            if '=' in line:
                line_split = line.replace('\n','').split('=')
                if line_split[0] == 'api_key':
                    api_key = line_split[1]

    if api_key == "":
        print("No API key found!")
        exit(1)

    bzapi = bugzilla.Bugzilla(URL, api_key)
    parser = argparse.ArgumentParser(description='Takes care of automatically update ITM, DTM, and adding MR comment to a bug')
    parser.add_argument('bz', type=int, nargs='+',
                        help='bz to handle')
    parser.add_argument('--mr', type=str, help='MR')
    parser.add_argument('--version', type=str, help='RHEL version, used by ITR/ZTR/DTM/ITM')
    args = parser.parse_args()
    bzs = args.bz
    mr = args.mr
    version = args.version
    print("version is %s" % version)
    for num in bzs:
        handle_bz(num, mr, version)