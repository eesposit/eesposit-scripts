#! /bin/bash

if [ $# -lt 3 ]
  then
    echo "Usage: $0 bz rhel_version downstream_target_branch"
    echo "bz: the BZ"
    echo "rhel_version: used by python bz for ITR/ZTR/DTM/ITM"
    echo "downstream_target_branch: branch name, could be main or 9.x 8.x"
    echo "Example: $0 123435 9.0 9.0"
    exit
fi

# Where is this script saved?
SCRIPTS_LOCATION=~/eesposit-scripts/bugzilla/

bz_number=$1
echo "BZ NUMBER: $bz_number"

rhel_version=$2
echo "RHEL VERSION: $rhel_version"

# could also be "main"
downstream_branch=$3
echo "Downstream target branch: $downstream_branch"

major_v=${downstream_branch:0:1}
# if version is the latest one (9.2,8.8), push to centos branches
if [ $major_v = "c" ] || [ $major_v = "m" ]
  then
  downstream_repo=origin
  MY_ORIGIN=eesposit
# if version is older (9.0, 9.1, 8.6, 8.7), push to rhelX branch
elif [ $major_v = "9" ]
  then
  downstream_repo=rhel9
  MY_ORIGIN=rhel9_me
elif [ $major_v = "8" ]
  then
  downstream_repo=rhel8
  MY_ORIGIN=rhel8_me
else
  echo "Unrecognized downstream target branch [valid are main, cXs, 8.X, 9.X]"
  exit
fi
echo "Downstream repo: $downstream_repo"
echo "MY REPO: $MY_ORIGIN"

echo "#### CHECK: BZ $bz_number for RHEL $rhel_version and MR in $downstream_repo/$downstream_branch"
echo "#### Are you sure?"
read answer
answer="${answer,,}"
if [[ ${answer:0:1} != "y" ]]; then
  echo "Exiting"
  exit
fi

my_branch=$(git branch --show-current)
echo "MR BRANCH: $my_branch"

echo "Pushing branch downstream..."
git push -u $MY_ORIGIN $my_branch
git push -f $MY_ORIGIN $my_branch

# echo "Preparing MR description... (if it takes a while, ensure that:
# 1. $downstream_branch is locally updated
# 2. this branch is rebased on local $downstream_branch"
git show -s > tempfile.txt
title=$(head -n 5 tempfile.txt | tail -n 1)
title=$(sed 's/^[[:space:]]*//' <<< "$title")

echo $title > tempfile.txt

echo "
Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=${bz_number}
" >> tempfile.txt

git log --pretty=format:"- %s [%an]" $downstream_branch..HEAD >> tempfile.txt

# echo "

# git-backport-diff output (for eventual conflicts, see the \"Conflicts\" section in the patch message itself):
# \`\`\`
# > git-backport-diff -u upstream/master -r $downstream_branch..HEAD -S -n" >> tempfile.txt

# upstream/master works with linux and qemu downstream repos
# bash ~/eesposit-scripts/git-scripts/git-backport-diff -u upstream/master -r $downstream_branch..HEAD -S -n >> tempfile.txt

# echo "
# \`\`\`

# Signed-off-by: Emanuele Giuseppe Esposito <eesposit@redhat.com>
# " >> tempfile.txt
echo "

Signed-off-by: Emanuele Giuseppe Esposito <eesposit@redhat.com>
" >> tempfile.txt

# Example output
# lab_req="https://gitlab.com/redhat/rhel/src/cloud-init/-/merge_requests/74/diffs"
echo "lab mr create $downstream_repo $downstream_branch -d -F tempfile.txt"
lab_req=$(lab mr create $downstream_repo $downstream_branch -d -F tempfile.txt)
# Test output in local repo
# lab_req=$(lab mr create $MY_ORIGIN $downstream_branch -d -F tempfile.txt)

rm tempfile.txt
echo $lab_req

mr=($(echo "$lab_req" | tr '/' '\n'))
mr=${mr[8]}

mrlink=$(dirname $lab_req)

echo "        lab created MR $mr"

cd $SCRIPTS_LOCATION

echo "python3 ~/eesposit-scripts/bugzilla/bugzilla_script.py $bz_number --mr $mrlink --version $rhel_version"
python3 bugzilla_script.py $bz_number --mr $mrlink --version $rhel_version