#! /bin/bash

# Usage: bbackport <upstream_sha>
# Same as backport(), but to use when we have cherry pick conflicts
bbackport() {
    echo "#####################################"
    echo "continuing backporting $1"
    echo "#####################################"
    if [ $# -lt 2 ]; then
		echo "Usage: bbackport <commit> <BZ>"
        return
	fi
    current_sha=$(git show --format=format:%H -s)
    right_sha=$(git show --format=format:%H -s $1)
    git show -s > tempfile.txt
    title=$(head -n 5 tempfile.txt | tail -n 1)
    title=$(sed 's/^[[:space:]]*//' <<< "$title")
    echo $title "\n" > tempfile.txt
    echo "Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=$2\n" >> tempfile.txt
    git show -s >> tempfile.txt
    sed -i "s/$current_sha/$right_sha/" tempfile.txt
    git commit --amend --reset-author -s -F tempfile.txt
    rm tempfile.txt
}

# Usage: backport <upstream_sha>
# backports upstream commit using git cherry-pick and
# then prepares a template file with the following format:
#
# Author: <you>
# Date: <now>
#
# Upstream commit title
#
#       commit <upstream_sha>
#       Author: <upstream author>
#       Date: <upstream commit date>
#
#       Upstream commit title
#
#       Upstream commit message
#
# Signed-off-by: <you
backport() {
    echo "#####################################"
    echo "backporting $1"
    echo "#####################################"
	if [ $# -lt 2 ]; then
		echo "Usage: backport <commit> <BZ>"
        return
	fi
    git cherry-pick $1
    bbackport $1 $2
}


