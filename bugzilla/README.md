# Purpose of the script

Suppose we have a BZ #XXXXXX, we need to backport it by:

1. Picking up the upstream commit and cherry pick it in our branch
2. Push our branch
3. Open a MR against the top of downstream branch (for example 8.7 and 9.1)
4. Update the BZ with MR link, moving it to POST, set DTM/ITR and ping right people to set `devel_ack+` and `qa_ack+`.

This process is pretty much trivial, so I wrote a couple of scripts to optimize it:

- `backport.sh`: provides commands to cherry-pick and prepare commit message
- `create_mr_bz.sh`: takes care of pushing the branch and creating a MR
- `bugzilla_script.py`: takes care of updating the BZ

# Usage and limitations

Some paths are hardcoded for my setup.

## Backporting

Suppose we have upstream commit `$UPSTREAM_COMMIT`:

	git show $UPSTREAM_COMMIT

	commit $UPSTREAM_COMMIT
	Author: <UPSTREAM AUTHOR>
	Date:   <UPSTREAM DATE>

		<UPSTREAM COMMIT TITLE>

		<UPSTREAM COMMIT MESSAGE>

	<DIFF>

### Usage
1. `source backport.sh` to include commands in the bash environment
2. create a new branch and run `backport $UPSTREAM_COMMIT`

This will create a new commit in the current branch in the following format:

	git show

	commit <NEW SHA>
	Author: <CURRENT AUTHOR>
	Date:   <NOW>

		<UPSTREAM COMMIT TITLE>

		commit $UPSTREAM_COMMIT
		Author: <UPSTREAM AUTHOR>
		Date:   <UPSTREAM DATE>

			<UPSTREAM COMMIT TITLE>

			<UPSTREAM COMMIT MESSAGE>

	Signed-off-by: <CURRENT AUTHOR>

	<DIFF>

In case `backport $UPSTREAM_COMMIT` does not backport the commit cleanly, use `bbackport $UPSTREAM_COMMIT` after cherry-pick conflict resolution is done and upstream commit has been picked.

## Opening a new MR

At this point, we want to open a new MR.

### Usage

`./create_mr_bz <BZ_NUMBER>`

Better when creating an alias in `.bashrc`:

	alias mr='bash ~/Desktop/cloud-init/eesposit-scripts/bugzilla/create_mr_bz.sh'

Here it is worth pointing out that I currently use `git worktree`, so each branch is in a folder with the downstream base branch name (`rhel-9.0.0` is in `cloudinit/rhel-9.0.0/`).

Therefore the script finds the downstream base branch by reading the folder name.
The variable to change are:

	# Currently new version is RHEL9, "old" is 8
	VERSION_OLD=8
	# Assignee for RHEL9 MR
	ASSIGNEE_R9="mrezanin"
	# Assignee for RHEL8 MR
	ASSIGNEE_R8="jmaloy"
	# git remote where to push my local branch
	MY_ORIGIN=eesposit
	# Where is this script saved?
	SCRIPTS_LOCATION=~/Desktop/cloud-init/eesposit-scripts/bugzilla/
	# Top branch for Centos (rhel 9 MR)
	CENTOS_BRANCH_TOP=c9s
	# Reviewers ID
	REVIEWERS="otubo vkuznets mgamal-rh"

There are 3 cases:

1. MR for a rhel 8 BZ: assignee is ASSIGNEE_R8, take branch name from folder
2. MR for a rhel 9 (Centos, latest) BZ: assignee is ASSIGNEE_R9, take branch name from `refs/remotes/origin`
3. MR for a rhel 9 (backport) BZ: assignee is ASSIGNEE_R9, take branch name from folder

Then the script:

- Pushes local branch on our remote
- Opens a MR using `lab`, using `$REVIEWERS`, `$ASSIGNEE` and our branch name. The MR text will be in this format:
```
	BZ: <NUMBER>
	Tested by: QA

	`output of git show`
```
- Thenk invokes `bugzilla_script.py BZ_NUMBER --mr MR_NUMBER --version BRANCH_NAME`

## BZ handling

Now we need to update the BZ. We want to:

1. Change status to POST
2. Write MR link as a private comment
3. Check `ZTR, DTM, ITR, ITM` are set correctly
4. Check and ping if `devel_ack+` and `qa_ack+` are missing

### Usage

`bugzilla_script.py BZ_NUMBER --mr MR_NUMBER --version BRANCH_NAME`

In 99% of the cases, this script is called automatically by `create_mr_bz.sh`

**IMPORTANT:** The script requires `python-bugzilla` version 3.2.0 or later.

The first thing that the script needs to do is to authenticate to `bugzilla.redhat.com`.
By default, the script looks in `~/.config/python-bugzilla/bugzillarc` for the line
`api_key=<API_KEY>`. No other method of authentication is allowed.

Default fields that can be changed:

	DEFAULT_PATH="~/.config/python-bugzilla/bugzillarc"

	default_component = "cloud-init"
	default_contact = 'virt-bugs@redhat.com'

	current_9_head = '9.1.0'
	current_8_head = '8.7.0'

	needinfo_setter = 'eesposit'

	PO = Person("Eduardo", "eterrel")
	QA = [Person("Amy", "xiachen"), Person("Huijuan", "huzhao")]

Then, it fetches the BZ. BZ must have component set to `default_component` and status to `NEW` or `ASSIGNED`.

Execution steps:

1. Fetch `ITR, DTM and ZTR` when available, and check branch provided from MR matches with `ITR` or `ZTR`. Otherwise exit, as we are modifying the wrong BZ.
2. Fetch `devel_ack, qa_ack and z_stream`.
3. If it's not a `z_stream` bug, ask on input an `ITR` and `DTM`.
4. Ping PO asking a `needinfo` if `devel_ack` is not set to `+`.
5. Ping `qa_contact` asking a `needinfo` if `qa_ack` is not set to `+`.
6. Set BZ status to `POST`.
7. Preview modifications (needinfo and status change). User must confirm before sending.
8. Preview comments. User must confirm before sending.