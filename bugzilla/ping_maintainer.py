import bugzilla
import os
import argparse
import pprint
import xmlrpc
import subprocess

enable_debug = False

def print_debug(string):
    if enable_debug:
        print(string)

URL='https://bugzilla.redhat.com/xmlrpc.cgi'
DEFAULT_PATH="~/.config/python-bugzilla/bugzillarc"

def ask_needinfo(requestee, obj):
    flag = {
        'requestee' : requestee,
        'name' : 'needinfo',
        'type_id': 16,
        'status' : '?',
        'new' : True,
    }

    if not "flags" in obj:
        obj["flags"] = []

    obj["flags"].append(flag)

def handle_bz(bz_number, rhel_branch, upstream_commit):
    bug = bzapi.getbug(bz_number)
    modifications = {}
    bug.refresh()
    print("\tFetched bug #%s" % bug.id)

    if not (bug.status == 'NEW' or bug.status == 'ASSIGNED'):
        print("#### Bug %s is not NEW or ASSIGNED, but %s!" % (bug.id, bug.status))
        exit(1)

    process = subprocess.run(['git', 'log', '-1', "--pretty=format:'%ae %an'"], capture_output=True, text=True)
    assert(process.stderr == '')
    maint = process.stdout.split()
    assert(len(maint) >= 2)
    maint_name = maint[1]
    maint_email = maint[0]

    comment_str = f"Hello {maint_name},\n I think you are one of \
rhel {rhel_branch} maintainers. This backport (upstream commit \
{upstream_commit}) applies cleanly in the {rhel_branch} branch, \
so if you want go ahead and do the MR yourself, feel free to \
do so. Otherwise I am happy to do it!\nThank you,\nEmanuele"
    ask_needinfo(maint_email, modifications)

    print("\nComments are:")
    pprint.pprint(comment_str)

    confirm = input("Do you want send comments?: ")

    if confirm[0] == 'y':
        print("Updated!")
        bug.addcomment(comment_str, private=True)
    else:
        print("Comments not sent!")


    print("\n\nModifications are:")
    pprint.pprint(modifications)

    confirm = input("Do you want to send modifications?: ")

    if confirm[0] == 'y':
        print("Updated!")
        bzapi.update_bugs([bz_number], modifications)
    else:
        print("Modifications not sent!")

if __name__=="__main__":
    path = os.path.expanduser(DEFAULT_PATH)
    api_key = ""
    with open(path) as f:
        for line in f.readlines():
            if '=' in line:
                line_split = line.replace('\n','').split('=')
                if line_split[0] == 'api_key':
                    api_key = line_split[1]

    if api_key == "":
        print("No API key found!")
        exit(1)

    bzapi = bugzilla.Bugzilla(URL, api_key)
    parser = argparse.ArgumentParser(description='Takes care of automatically update ITM, DTM, and adding MR comment to a bug')
    parser.add_argument('bz', type=int,
                        help='bz to handle')
    parser.add_argument('rhel_branch', type=str, help='rhel downstream branch')
    parser.add_argument('upstream_commit', type=str, help='linux upstream commit')
    args = parser.parse_args()
    bz = args.bz
    rhel_branch = args.rhel_branch
    upstream_commit = args.upstream_commit
    handle_bz(bz, rhel_branch, upstream_commit)