#! /bin/bash -e

setopt rmstarsilent 2> /dev/null

rm /home/eesposit/.ssh/id_redhat*
rm /home/eesposit/.ssh/beaker_machines*

rm /home/eesposit/.config/lab/lab.toml
sudo rm /etc/krb5.keytab || true
sudo rm -rf /etc/ipa || true
rm /home/eesposit/.config/python-bugzilla/bugzillarc
rm /home/eesposit/.gitconfig
userdel -f eesposit