#! /bin/bash -e

# Executed locally in my laptop to copy stuff in the machine I want to setup

USERNAME=eesposit

if [ -z $1 ]; then
    echo "Usage: $0 <ip> <user> [privilege]
"
    echo "ip: something like virtlab###.virt.lab.eng.bos.redhat.com"
    echo "user: user of machine, usually root"
    echo "privilege: if kerberos, bugzilla and lab should be installed"
    exit 1
fi

if [ $# -ge 2 ]; then
    USERNAME=$2
fi

pw=`cat .eesposit_pw`
scp create_eesposit.sh $USERNAME@$1:~/
scp ~/.ssh/beaker_machines.pub $USERNAME@$1:~/
ssh -t $USERNAME@$1 'bash create_eesposit.sh'
ssh -t $USERNAME@$1 "sudo echo $pw | passwd --stdin eesposit"
# add beaker key to ssh agent, so we can login to eesposit user
eval $(ssh-agent -s)
ssh-add ~/.ssh/beaker_machines

USERNAME=eesposit

ssh $USERNAME@$1 'mkdir -p downloads'

# ssh key for github
ssh -t $USERNAME@$1 'sudo dnf install -y git'
scp ~/.ssh/id_redhat $USERNAME@$1:~/.ssh/
scp ~/.ssh/id_redhat.pub $USERNAME@$1:~/.ssh/
scp ~/.gitconfig $USERNAME@$1:~/.gitconfig

# ssh key for beaker
scp ~/.ssh/beaker_machines $USERNAME@$1:~/.ssh/
scp ~/.ssh/beaker_machines.pub $USERNAME@$1:~/.ssh/

if [ $# -ge 3 ]; then
    echo "installing also kerberos, bugzilla and lab"
    # for kerberos login (rest of packages are in install_packages.sh)
    scp /etc/krb5.keytab.tocopy $USERNAME@$1:/etc/krb5.keytab
    ssh -t $USERNAME@$1 'sudo rm /etc/krb5.conf'
    scp /etc/krb5.conf $USERNAME@$1:/etc/krb5.conf

    # copy lab stuff
    ssh -t $USERNAME@$1 'mkdir -p .config/lab/'
    scp ~/.config/lab/lab.toml $USERNAME@$1:~/.config/lab/
    ssh -t $USERNAME@$1 'chmod 600 .config/lab/lab.toml'

    # copy bugzilla script stuff
    ssh -t $USERNAME@$1 'mkdir -p .config/python-bugzilla/'
    scp ~/.config/python-bugzilla/bugzillarc $USERNAME@$1:~/.config/python-bugzilla/
    ssh -t $USERNAME@$1 'chmod 600 .config/python-bugzilla/bugzillarc'
fi

# install eesposit-scripts
ssh -t $USERNAME@$1 'git clone https://gitlab.com/eesposit/eesposit-scripts.git && echo "source ~/eesposit-scripts/zshrc_aliases" >> ~/.bashrc' || true
# ssh -t $USERNAME@$1 'cd eesposit-scripts && git clone https://github.com/codyprime/git-scripts.git'
if [ $# -ge 3 ]; then
    ssh -t $USERNAME@$1 'bash ~/eesposit-scripts/beaker_setup/beaker_init.sh lab kerberos'
fi
ssh -t $USERNAME@$1 'bash ~/eesposit-scripts/beaker_setup/beaker_init.sh help'

ssh $USERNAME@$1
