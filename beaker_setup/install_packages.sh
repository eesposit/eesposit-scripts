#! /bin/bash -e

# necessary
sudo yum install -y git nano make gcc gdb gdb-gdbserver rsync wget zsh glib2-devel python3 python3-devel python3-pip valgrind > /dev/null || true

# qemu-related
sudo yum install -y qemu-kvm libvirt libvirt-daemon libguestfs-tools cscope tigervnc tigervnc-server virt-install libseccomp libseccomp-devel meson > /dev/null || true

# risky ones, might fail
sudo yum install -y epel-release epel-next-release > /dev/null || true

# linux-related
sudo yum install -y flex bison elfutils-libelf-devel openssl-devel openssl-devel-engine dwarves bc krb5-workstation alsa-lib-devel glibc-devel.*i686 ncurses-devel msr-tools cpuid binutils kernel-devel > /dev/null || true

# kerberos
sudo yum install -y openssh-clients koji brewkoji rhpkg > /dev/null || true

# ??
sudo yum install -y pixman-devel zlib-devel libaio-devel libcap-devel libxml2-devel libmnl rpmdevtools rpmlint swtpm swtpm-tools go > /dev/null || true

sudo yum install -y guestfs-tools automake numactl sysstat gnu-efi > /dev/null || true

# systemd
sudo yum install -y gnu-efi gnu-efi-devel gperf edk2-ovmf shim efibootmgr > /dev/null || true
sudo yum install -y pesign mokutil keyutils vim-common > /dev/null || true

# dracut
sudo dnf install -y kmod kmod-devel asciidoc virt-firmware genisoimage squashfs-tools lvm2 tpm2-abrmd tpm2-tools > /dev/null || true

pip3 install --user mypy pylint flake8 b4 bugzilla virtualenv > /dev/null || true

# not sure they actually exist
sudo yum install -y libiscsi-devel libfdt-devel ninja-build libmnl-devel > /dev/null || true

sudo yum install -y yum-utils > /dev/null || true
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo > /dev/null || true
sudo yum -y install packer > /dev/null || true
packer plugins install github.com/hashicorp/qemu > /dev/null || true

# kmod
sudo dnf install -y gtk-doc libtool veritysetup systemd-ukify

pip3 install --user jinja2 pyelftools > /dev/null || true
