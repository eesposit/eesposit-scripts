#! /bin/bash -e

id -u eesposit &>/dev/null || adduser -m eesposit
mkdir /home/eesposit/.ssh
mv beaker_machines.pub /home/eesposit/.ssh/authorized_keys
chown eesposit:eesposit -R /home/eesposit
chmod 600 /home/eesposit/.ssh/authorized_keys
sed '/^root.*ALL=(ALL).*/ a eesposit  ALL=(ALL)       NOPASSWD: ALL' /etc/sudoers > sudoers
chmod 440 sudoers
mv sudoers /etc/sudoers