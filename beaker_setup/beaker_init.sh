#! /bin/bash -e

# Executed in a machine that needs to have all repos and everything set up

# chmod +x install_packages.sh
# ./install_packages.sh || true

declare -A commands

# returns true if file DOES NOT exist
check_file_exist () {
	filename=$1
	comm=$2

	if [ ! -f "$filename" ]; then
		echo "$filename does not exist. Install with \"setup_machine $comm\""
		return 0
	else
		return 1
	fi
}

# lab
install_lab () {
	LAB_FILE=~/.config/lab/lab.toml
	if check_file_exist $LAB_FILE lab; then
		return 0
	fi

	echo "Installing lab..."
	sudo dnf copr enable -y bmeneguele/rhkernel-devtools epel-8-x86_64
	sudo dnf install -y lab
}
commands["lab"]=install_lab

# kerberos
install_kerberos () {
	KERB_FILE=/etc/krb5.keytab
	if check_file_exist $KERB_FILE lab; then
		return 0
	fi

	echo "Installing kerberos certs..."
	chmod 600 /etc/krb5.keytab
	chmod 644 /etc/krb5.conf
	sudo mkdir -p /etc/ipa
	sudo curl -o /etc/ipa/ca.crt https://password.corp.redhat.com/ipa.crt
	wget http://hdn.corp.redhat.com/rhel7-csb-stage/RPMS/noarch/redhat-internal-cert-install-0.1-25.el7.noarch.rpm
	sudo rpm -i redhat-internal-cert-install-0.1-25.el7.noarch.rpm
	rm redhat-internal-cert-install-0.1-25.el7.noarch.rpm
	cd ~
}
commands["kerberos"]=install_kerberos

# ssh keys for github
setup_github () {
	cd ~/.ssh
	chmod 600 id_redhat
	chmod 600 beaker_machines
	echo "Host gitlab.com
	User eesposit
	IdentityFile ~/.ssh/id_redhat" >> ~/.ssh/config
	echo "Host github.com
	User eesposit
	IdentityFile ~/.ssh/id_redhat" >> ~/.ssh/config
	chmod 644 ~/.ssh/config

	# gitlab known_host, so it doesn't ask: do you know this host?
	echo "gitlab.com,172.65.251.78 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
	chmod 644 known_hosts

	# ssh key to access github
	eval $(ssh-agent -s)
	ssh-add id_redhat
	ssh-add beaker_machines
	ssh -T git@gitlab.com
	ssh -T git@github.com
	cd ~
}
commands["git"]=setup_github

clone_linux () {
	mkdir -p ~/kernel
	cd ~/kernel

	# download linux. Do not compile it because you have to set KVM_INTEL/AMD to = m
	git clone https://github.com/torvalds/linux.git
	cd linux
	git remote add eesposit git@gitlab.com:eesposit/linux.git
	git remote add kvm https://git.kernel.org/pub/scm/virt/kvm/kvm.git
	make localyesconfig
	cd ~
}
commands["linux"]=clone_linux

clone_r8 () {
	mkdir -p ~/kernel
	cd ~/kernel
	git clone git@gitlab.com:eesposit/kernel-centos-stream-8.git kernel-rhel-8
	cd kernel-rhel-8
	git remote add rhel8 git@gitlab.com:redhat/rhel/src/kernel/rhel-8.git
	git remote add rhel8_me git@gitlab.com:eesposit/kernel-rhel-8.git
	git remote add eesposit git@gitlab.com:eesposit/kernel-centos-stream-8.git
	git remote add kvm https://git.kernel.org/pub/scm/virt/kvm/kvm.git
	git remote add upstream https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
	cd ~
}
commands["r8"]=clone_r8

clone_r9 () {
	mkdir -p ~/kernel
	cd ~/kernel
	git clone git@gitlab.com:eesposit/kernel-centos-stream-9.git kernel-rhel-9
	cd kernel-rhel-9
	git remote add rhel9 git@gitlab.com:redhat/rhel/src/kernel/rhel-9.git
	git remote add rhel9_me git@gitlab.com:eesposit/kernel-rhel-9.git
	git remote add eesposit git@gitlab.com:eesposit/kernel-centos-stream-9.git
	git remote add kvm https://git.kernel.org/pub/scm/virt/kvm/kvm.git
	git remote add upstream https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
	cd ~
}
commands["r9"]=clone_r9

# kvm selftests
clone_selftests () {
	mkdir -p ~/kernel
	cd ~/kernel
	git clone https://gitlab.com/kvm-unit-tests/kvm-unit-tests.git
	cd kvm-unit-tests
	git remote add eesposit git@gitlab.com:eesposit/kvm-unit-tests.git
	./configure
	make -j40
	cd ~
}
commands["self"]=clone_selftests


# qemu
clone_qemu_up () {
	mkdir -p ~/qemu
	cd ~/qemu
	git clone https://git.qemu.org/git/qemu.git
	cd qemu
	git config diff.orderFile scripts/git.orderfile
	git remote add bonzini git@gitlab.com:bonzini/qemu.git
	git fetch bonzini
	git remote add eesposit git@gitlab.com:eesposit/qemu.git
	git fetch eesposit
	git remote add kwolf https://repo.or.cz/qemu/kevin.git
	git fetch kwolf
	mkdir build
	cd build
	../configure  --target-list=x86_64-softmmu
	make -j40
	cd ~
}
commands["qemu"]=clone_qemu_up

clone_qemu_c9 () {
	mkdir -p ~/qemu
	cd ~/qemu
	git clone git@gitlab.com:redhat/centos-stream/src/qemu-kvm.git qemu-kvm-centos
	cd qemu-kvm-centos
	git config diff.orderFile scripts/git.orderfile
	git remote add eesposit git@gitlab.com:eesposit/qemu-kvm.git
	git remote add upstream https://git.qemu.org/git/qemu.git
	# git remote add rhel9 git@gitlab.com:redhat/rhel/src/qemu-kvm/qemu-kvm.git
	# git remote add rhel9_me git@gitlab.com:eesposit/qemu-kvm-rhel.git
	cd ~
}
commands["qemu_c9"]=clone_qemu_c9

clone_qemu_down () {
	mkdir -p ~/qemu
	cd ~/qemu
	git clone git@gitlab.com:redhat/rhel/src/qemu-kvm/qemu-kvm.git qemu-kvm-rhel
	cd qemu-kvm-rhel
	git config diff.orderFile scripts/git.orderfile
	git remote add eesposit git@gitlab.com:eesposit/qemu-kvm-rhel.git
	git remote add upstream https://git.qemu.org/git/qemu.git
	cd ~
}
commands["qemu_c8"]=clone_qemu_down

change_zsh () {
	cd ~
	# change default shell
	sudo usermod --shell /bin/zsh `whoami`

	# install oh-my-zsh
	sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" &
	proc=$!
	sleep 2
	kill $proc || echo "could not kill"
	sed -i -e '0,/robbyrussell/{s/robbyrussell/agnoster/}' ~/.zshrc
	sed -i -e '0,/# DISABLE_UPDATE_PROMPT="true"/{s/# DISABLE_UPDATE_PROMPT="true"/DISABLE_UPDATE_PROMPT="true"/}' ~/.zshrc

	# write aliases to .zshrc
	echo "source ~/eesposit-scripts/zshrc_aliases" >> ~/.zshrc

	source ~/.zshrc
}
commands["zsh"]=change_zsh

# test_fn () {
# 	echo "asddsadasd"
# }
# commands["test"]=test_fn

# test_fn2 () {
# 	echo "asddsadasd"
# }
# commands["test2"]=test_fn2

help_fn () {
	echo "Available commands:"
	for l in "${!commands[@]}"; do
		echo "    $l --> ${commands[$l]}";
	done
	exit
}
commands["help"]=help_fn

if [ -z  $1 ]; then
	help_fn
fi

for i in "$@"; do
	if [ ${commands[$i]+_} ]; then
    		echo "Executing $i...";
		${commands[$i]}
	else
		echo "$i not found";
	fi
done

echo "Done setting up the machine!"

## in caso non funziona dnf
# cat /etc/yum.repos.d/rhel9.repo
# [rhel9-base]
# baseurl=http://download.devel.redhat.com/rhel-9/composes/RHEL-9/RHEL-9.2.0-20230118.d.5/compose/BaseOS/x86_64/os/
# enabled=1
# gpgcheck=0
# skip_if_unavailable=1

# [rhel9-appstream]
# baseurl=http://download.devel.redhat.com/rhel-9/composes/RHEL-9/RHEL-9.2.0-20230118.d.5/compose/AppStream/x86_64/os/
# enabled=1
# gpgcheck=0
# skip_if_unavailable=1


# kernel stuff

# LPATH="../linux/"
# KVER=`cat ${LPATH}include/config/kernel.release`
# echo "Installing kernel:"
# echo $KVER
# echo "Current kernel version:"
# ssh $USER@$1 uname -r

# cd $LPATH

# make -j40
# make modules -j40
# rsync -av --progress arch/x86/boot/bzImage $USER@$1:/tmp/vmlinuz-$KVER
# rsync -av --progress System.map $USER@$1:/tmp/System.map-$KVER
# [ ! -d /tmp/$KVER/lib/firmware ] && mkdir -p /tmp/$KVER/lib/firmware ||:

# INSTALL_MOD_PATH=/tmp/$KVER make INSTALL_MOD_STRIP=1 modules_install
# rsync -av /tmp/$KVER/lib/modules/$KVER $USER@$1:/lib/modules/
# ssh $USER@$1 installkernel $KVER /tmp/vmlinuz-$KVER /tmp/System.map-$KVER
# ssh $USER@$1 sudo chmod 755 /boot/vmlinuz-$KVER

# # install the kernel
# ssh $USER@$1 grub2-mkconfig -o /boot/grub2/grub.cfg
# ssh $USER@$1 grubby --set-default "/boot/vmlinuz-$KVER"

#ssh $USER@$1 reboot