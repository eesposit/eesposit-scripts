#! /bin/bash -e

if [[ -z "${BSERVER}" ]]; then
    # BSERVER=amd1.3a2m.lab.eng.bos.redhat.com
    # echo "Defaulting server to ${BSERVER}"
    echo "Provide server"
    exit 1
else
    echo "Connecting to ${BSERVER}"
fi

LPATH="../linux/"
cd $LPATH
make modules -j40

ssh root@$BSERVER 'mkdir -p modules'
ssh root@$BSERVER 'rm -f ~/modules/*'

set +e
ssh root@$BSERVER 'sudo rmmod kvm_intel && sudo rmmod kvm && sudo rmmod irqbypass'
set -e

scp virt/lib/irqbypass.ko root@$BSERVER:~/modules
scp arch/x86/kvm/kvm.ko root@$BSERVER:~/modules
scp arch/x86/kvm/kvm-intel.ko root@$BSERVER:~/modules

ssh root@$BSERVER 'sudo insmod ~/modules/irqbypass.ko && sudo insmod ~/modules/kvm.ko && sudo insmod ~/modules/kvm-intel.ko'
