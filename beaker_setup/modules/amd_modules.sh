#! /bin/bash -e

if [[ -z "${BSERVER}" ]]; then
    # BSERVER=amd1.3a2m.lab.eng.bos.redhat.com
    # echo "Defaulting server to ${BSERVER}"
    echo "Provide server"
    exit 1
else
    echo "Connecting to ${BSERVER}"
fi

LPATH="../linux/"
cd $LPATH
make modules -j40

ssh root@$BSERVER 'mkdir -p modules'
ssh root@$BSERVER 'rm -f ~/modules/*'

set +e
ssh root@$BSERVER 'sudo rmmod kvm_amd'
set -e

scp arch/x86/kvm/kvm-amd.ko root@$BSERVER:~/modules

ssh root@$BSERVER 'sudo insmod ~/modules/kvm-amd.ko'
