#! /bin/bash
set -e

# THIS WORKS ON BRANCH dev-preview

echo "########## DELETE HELLO ##########"
oc delete all -l app=hello-openshift
echo "########## DELETE KATACONFIG CR ##########"
oc delete -f config/samples/kataconfiguration_v1_kataconfig.yaml
echo "########## DELETE CONFIG/PEERPODS ##########"
oc delete --ignore-not-found=true -k config/peerpods/
echo "########## DELETE CERT MANAGER ##########"
oc delete --ignore-not-found=true -f https://github.com/cert-manager/cert-manager/releases/download/v1.10.1/cert-manager.yaml