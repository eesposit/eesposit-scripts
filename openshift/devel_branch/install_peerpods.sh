! /bin/bash
set -e

echo "########## CERT MANAGER ##########"
oc apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.10.1/cert-manager.yaml
sleep 5
oc get all -n cert-manager
echo "########## CONFIG/PEERPODS ##########"
cd ~/sandboxed-containers-operator
oc apply -k config/peerpods
echo "##### Waiting..."
while true; do
	x=`oc get mcp/worker | awk -F" " 'FNR == 2 { print $7 }'`
	sleep 5
	echo "value is $x"
	if [ $x == 2 ]; then
		echo "##### Done!"
		break
	fi
done
echo "########## SECRET ##########"
oc create secret generic auth-json-secret --from-literal=auth.json=$(oc get -n openshift-config secret/pull-secret -ojson | jq -r '.data.".dockerconfigjson" | @base64d') -n openshift-sandboxed-containers-operator
kubectl create secret generic ssh-key-secret --from-file=id_rsa=/home/eesposit/.ssh/id_rsa -n openshift-sandboxed-containers-operator
echo "########## KATACONFIG CR ##########"
oc apply -f config/samples/kataconfiguration_v1_kataconfig.yaml
echo "##### Waiting..."
while true; do
	x=`oc get mcp/kata-oc | awk -F" " 'FNR == 2 { print $7 }'`
	sleep 5
	echo "value is $x"
	if [ $x == 2 ]; then
		echo "##### Done!"
		break
	fi
done
echo "########## CHECKING ##########"
oc get pods -n openshift-sandboxed-containers-operator
oc get pods -n peer-pods-webhook-system
echo "########## HELLO WORLD ##########"
cd ~
oc apply -f hello-openshift.yaml
oc expose service hello-openshift-service -l app=hello-openshift
oc get pod/hello-openshift
APP_URL=$(oc get routes/hello-openshift-service -o jsonpath='{.spec.host}')
curl ${APP_URL}